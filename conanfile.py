from conan import ConanFile

class TraitPointPlanRecipe(ConanFile):
    name = "traitpointplan"
    executable = "ds_TraitPointPlan"
    version = "2.3.0"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Florent Langlois"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/motion/traitpointplan.git"
    description = "TraitPointPlan device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        
        self.requires("utils/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
