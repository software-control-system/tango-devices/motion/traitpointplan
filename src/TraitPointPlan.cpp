static const char *RcsId = "$Header: /users/chaize/newsvn/cvsroot/Motion/TraitPointPlan/src/TraitPointPlan.cpp,v 1.58 2012-01-13 18:51:38 buteau Exp $";
//+=============================================================================
//
// file :         TraitPointPlan.cpp
//
// description :  C++ source for the TraitPointPlan and its commands. 
//                The class is derived from Device. It represents the
//                CORBA servant object which will be accessed from the
//                network. All commands which can be executed on the
//                TraitPointPlan are implemented in this file.
//
// project :      TANGO Device Server
//
// $Author: buteau $
//
// $Revision: 1.58 $
//
// $Log: not supported by cvs2svn $
// Revision 1.57  2011/11/25 16:42:47  jcpret
// Added default value for AngleUnit property
//
// Revision 1.56  2011/11/23 13:43:08  jcpret
// Fixed default values setting issue (see Bug 20733)
//
// Revision 1.54  2011/11/21 09:51:39  jcpret
// Fixed Mantis bug 20733: Angle unit can now be Deg or mRad (defined by AngleUnit property)
//
// Revision 1.53  2011/10/21 15:25:18  buteau
// Fix leak m�moire sur read_Zc
//
// Revision 1.52  2011/10/03 14:06:26  jcpret
// Removed InitializeReferencePosition Command (as requested by Mantis bug 19992)
//
// Revision 1.51  2011/09/12 13:01:33  jcpret
// Correstions after on site tests (see Mantis bug 18280)
//
// Revision 1.50  2011/09/08 14:12:54  buteau
// TEST 8/09/2011 sur SIXS
//
// Revision 1.49  2011/08/18 16:59:30  jcpret
// Adapted state management to AttributeGroup use (i.e. considered the case where several motors are on the same device)
//
// Revision 1.48  2011/08/09 14:48:46  jcpret
// Implementation of the recommendations defined in bug 18280
//
// Revision 1.47  2011/08/05 15:27:29  piccaf
// test SOLEIL 05/08/2011
//
// Revision 1.46  2011/08/02 09:50:01  jcpret
// first step of attributegroup integration
//
// Revision 1.45  2010/08/12 17:14:07  flanglois
// - set write part of attributes (mantis: 0015464)
// - cleaning in code
//
// Revision 1.44  2010/03/26 10:01:20  vince_soleil
// "Migration_Tango7_Part2"
//
// Revision 1.43  2007/10/04 12:09:15  flanglois
// - fixed bug: getting bounds
//
// Revision 1.42  2007/07/25 16:16:32  stephpsoleil
// Relaunching some exceptions for upper devices
//
// Revision 1.41  2007/07/23 15:17:37  stephpsoleil
// Updating VC6 and VC8 workspaces
//
// Revision 1.40  2007/06/19 16:51:21  stephpsoleil
// Correcting bugs from Proxima Beamline
//
// Revision 1.39  2007/06/11 13:04:12  stephpsoleil
// Adding try catch block to secure the program
//
// Revision 1.38  2007/06/05 12:05:47  stephpsoleil
// Adding Visual C++ 2005 workspace for Datafitter test device.
// Modifying sources to remove warning in visual 2005
//
// Revision 1.37  2007/05/29 14:12:41  stephpsoleil
// Minor correction for linux compile
//
// Revision 1.36  2007/05/29 10:08:53  stephpsoleil
// Auto creating properties in Jive if the property doesn't exist.
//
// Revision 1.35  2007/05/25 12:05:13  stephpsoleil
// Adding FirstInit property to create properties in Jive at the first device launch
//
// Updating documentation
//
// Revision 1.34  2007/05/11 17:02:18  stephpsoleil
// Exception error correction
//
// Revision 1.33  2007/04/13 07:59:18  stephpsoleil
// Devices State display updating (now all the database name is entered)
//
// Revision 1.32  2007/03/19 14:09:37  stephpsoleil
// Merge conflict resolution
//
// Revision 1.31  2007/03/19 11:21:34  stephpsoleil
// StateMachine verification and correction
// Status message improved : show the state of the reached device and the lost of communication
// Description of the properties, attributes, command and state.
// Removing eclipse workspace directory (now in the root of the directory)
//
// Revision 1.30  2007/03/16 14:55:06  stephpsoleil
// Dev_State improvment
// Description of the state of each proxy to a device or a motor
//
// Revision 1.29  2007/01/25 09:19:39  stephpsoleil
// Initialization modification
// Now if it possible the initialize method is called automatically if the creation of the proxies is ok
//
// Improvment of the verification of the init properties
//
// Modification of the dev_state
//
// Revision 1.28  2007/01/22 14:58:09  stephpsoleil
// Initialization modification
// Now if it possible the initialize_TPP method is called automatically if the creation of the proxies is ok
//
// Improvment of the verification of the init properties
// Mainly for the 3 or 5 motors property
//
// Revision 1.27  2007/01/22 13:12:26  stephpsoleil
// Initialization modification
// Now if it possible the initialize_TPP method is called automatically if the creation of the proxies is ok
//
// Revision 1.26  2007/01/16 14:11:07  stephpsoleil
// Adding YawDirection properties
//
// Revision 1.25  2006/12/20 14:33:46  stephpsoleil
// Updating Eclipse Workspace
//
// Revision 1.24  2006/12/07 09:46:39  stephpsoleil
// In the initXXX method rethrow of the Exception in order to be well catched in the initialize method
// Useful to indicate if the initialization of the differents object of the lib are ok or not
//
// Revision 1.23  2006/12/05 11:02:07  stephpsoleil
// SaveNominal and LoadNominal correction
//
// Revision 1.22  2006/12/05 09:28:23  stephpsoleil
// Modification of the throwing signature to compile under Linux
//
// Revision 1.21  2006/12/05 08:52:21  stephpsoleil
// Correction of the computing of Zc
//
// Revision 1.20  2006/12/04 16:17:32  stephpsoleil
// Correction of the computing of Zc
//
// Revision 1.19  2006/12/04 13:12:51  stephpsoleil
// Removing doc + dev_state modification
//
// Revision 1.18  2006/12/01 15:59:50  stephpsoleil
// Update of the StateMachine
//
// Revision 1.17  2006/12/01 15:28:44  stephpsoleil
// New version of TPP to correct non coherent behaviour
//
// Revision 1.16  2006/11/27 10:58:32  stephpsoleil
// Minor Modif
//
// Revision 1.15  2006/11/27 09:14:10  stephpsoleil
// Correction of a bug when stop command is applied.
// Roll and pitch move together whereas only one command was launched.
//
// Revision 1.14  2006/11/23 13:53:02  stephpsoleil
// Minor corrections for the boundaries conditions
//
// Revision 1.13  2006/11/22 15:19:54  flanglois
// - modif makefile (still todo)
// - Xstring sous linux
//
// Revision 1.12  2006/11/21 14:03:11  stephpsoleil
// Correction of the status
//
// Revision 1.11  2006/11/21 12:58:07  stephpsoleil
// Modification for the min max values of device attribute
// Adding Config file
//
// Revision 1.10  2006/10/27 13:12:11  stephpsoleil
// Putting all possible attributes to memorized and removing write_hardware_at_init
//
// Defaults properties values are set when possible
//
// Revision 1.9  2006/07/06 08:22:09  stephpsoleil
// Sources update
//
// Revision 1.8  2006/06/27 13:31:44  stephpsoleil
// Updating makefiles and workspace
//
// Revision 1.7  2006/06/27 13:31:31  stephpsoleil
// Updating makefiles and workspace
//
// Revision 1.6  2006/06/02 15:07:28  stephpsoleil
// Adding scripts files
//
// Updating source
//
// Revision 1.5  2006/03/29 09:18:09  stephpsoleil
// Linux Commit
// Modification of the Makefile and the sources in order to compile under Linux
//
// Revision 1.4  2006/03/23 09:51:22  stephpsoleil
// Added InitializeReferencePosition method
// Files Added
// Makefile.linux
// Scripts to launch the Motors and the TPP
// Script to launch AtkTuning
// Workspace Visual C++ 6.0
//
// Revision 1.3  2006/01/24 18:19:51  stephpsoleil
// Ajout de la doc.
// Modif �quations
// --> ajout sensRoll et sensPitch
// Correction du code (test sur TPP Samba)
//
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================



//===================================================================
//
//	The following table gives the correspondence
//	between commands and method name.
//
//  Command name              |  Method name
//	----------------------------------------
//  State                     |  dev_state()
//  Status                    |  dev_status()
//  SaveAsNominal             |  save_as_nominal()
//  GoToNominal               |  go_to_nominal()
//  Stop                      |  stop()
//  Undo                      |  undo()
//  MotorsToZero              |  motors_to_zero()
//  InitializeTraitPointPlan  |  initialize_trait_point_plan()
//
//===================================================================


#include <TraitPointPlan.h>
#include <TraitPointPlanClass.h>
#include "PogoHelper.h"
#include "helpers/XString.h"
#include "TraitPointPlanConfig.h"

#include "DivideByZeroException.h"
#include "Tools.h"

#include <yat4tango/InnerAppender.h>

#define MIN_PROPERTIES_VALUE -1000000
#define MAX_PROPERTIES_VALUE 1000000
const double THOUSAND=1000.0;
const double PI = 3.1415927;
const double HALF_CIRCLE = 180.0;
const double NAN_VAL  = ::sqrt((double)-1);


namespace TraitPointPlan_ns
{

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::TraitPointPlan(string &s)
	// 
	// description : 	constructor for simulated TraitPointPlan
	//
	// in : - cl : Pointer to the DeviceClass object
	//      - s : Device name 
	//
	//-----------------------------------------------------------------------------
	TraitPointPlan::TraitPointPlan(Tango::DeviceClass *cl,string &s)
		:Tango::Device_4Impl(cl,s.c_str()), tpp_attr_grp( this ), m_initialised_group( false )
	{
		init_device();
	}

	TraitPointPlan::TraitPointPlan(Tango::DeviceClass *cl,const char *s)
		:Tango::Device_4Impl(cl,s), tpp_attr_grp( this ), m_initialised_group( false )
	{
		init_device();
	}

	TraitPointPlan::TraitPointPlan(Tango::DeviceClass *cl,const char *s,const char *d)
		:Tango::Device_4Impl(cl,s,d), tpp_attr_grp( this ), m_initialised_group( false )
	{
		init_device();
	}
	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::delete_device()
	// 
	// description : 	will be called at device destruction or at init command.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::delete_device()
	{
		//	Delete device's allocated object	

		if (iType != -1)
		{
			DELETE_SCALAR_ATTRIBUTE(attr_pitch_read);
			DELETE_SCALAR_ATTRIBUTE(attr_zC_read);
			DELETE_SCALAR_ATTRIBUTE(attr_t1z_read);
			DELETE_SCALAR_ATTRIBUTE(attr_t2z_read);
			DELETE_SCALAR_ATTRIBUTE(attr_roll_read);
			DELETE_SCALAR_ATTRIBUTE(attr_t3z_read);
			DELETE_SCALAR_ATTRIBUTE(attr_yaw_read);
			DELETE_SCALAR_ATTRIBUTE(attr_xC_read);
			DELETE_SCALAR_ATTRIBUTE(attr_t4x_read);
			DELETE_SCALAR_ATTRIBUTE(attr_t5x_read);
		}

		yat4tango::InnerAppender::release(this);
	}

	//-----------------------------------------------------------------------------
	double TraitPointPlan::getMinValue(std::string sAttributeName)
	{
		try
		{
			Tango::AttributeConfig	aConfig;
			Tango::Attribute& attr	= dev_attr->get_attr_by_name(sAttributeName.c_str());
			attr.get_properties(aConfig);
			if (string(aConfig.min_value).find("Not specified") ==  string::npos)
			{
				return XString<double>::convertFromString(string(aConfig.min_value));
			}
			else 
			{
				return MIN_PROPERTIES_VALUE;
			}
		}
		catch(Tango::DevFailed& e)
		{	
			ERROR_STREAM << e << ENDLOG;
		}
		catch(...)
		{
			ERROR_STREAM << "Unknown Error in getMinValue." << endl;
		}
		return MIN_PROPERTIES_VALUE;
	}


	//-----------------------------------------------------------------------------
	double TraitPointPlan::getMaxValue(std::string sAttributeName)
	{
		try
		{
			Tango::AttributeConfig	aConfig;
			Tango::Attribute& attr	= dev_attr->get_attr_by_name(sAttributeName.c_str());
			attr.get_properties(aConfig);
			if (string(aConfig.max_value).find("Not specified") == string::npos)
			{
				return XString<double>::convertFromString(string(aConfig.max_value));
			}
			else 
			{
				return MAX_PROPERTIES_VALUE;
			}
		}
		catch(Tango::DevFailed& e)
		{	
			ERROR_STREAM << e << endl;
		}
		catch(...)
		{
			ERROR_STREAM << "Unknown Error in getMaxValue." << endl;
		}
		return MAX_PROPERTIES_VALUE;
	}
	//+------------------------------------------------------------------

	int TraitPointPlan::FindIndexFromPropertyName(Tango::DbData& dev_prop, string property_name)
	{
		size_t iNbProperties = dev_prop.size();
		unsigned int i;
		for (i=0;i<iNbProperties;i++)
		{
			std::string sPropertyName(dev_prop[i].name);
			if (sPropertyName == property_name) return i;
		}

		if (i == iNbProperties) return -1;
		return i;
	}
	//+------------------------------------------------------------------


	void TraitPointPlan::store_value_as_property(Tango::DbData& dev_prop,Tango::DevString value,string property_name)
	{

		int iPropertyIndex = FindIndexFromPropertyName(dev_prop,property_name);

		if (iPropertyIndex == -1) return;

		if (dev_prop[iPropertyIndex].is_empty())
		{
			Tango::DbDatum current_value(dev_prop[iPropertyIndex].name);
			current_value << value;

			Tango::DbData db_data;
			db_data.push_back(current_value);

			try
			{
				get_db_device()->put_property(db_data);
			}
			catch(Tango::DevFailed &e)
			{
				string message= "Error in storing " + property_name + " in Configuration DataBase ";
				LOG_ERROR((message));
				ERROR_STREAM << e << ENDLOG;
			}
		}
	}
	//+------------------------------------------------------------------

	void TraitPointPlan::read3MotorsBounds()
	{
		dMinimalRoll	= getMinValue("roll");
		dMaximalRoll	= getMaxValue("roll");
		dMinimalPitch	= getMinValue("pitch");
		dMaximalPitch	= getMaxValue("pitch");
		dMinimalT1z	= getMinValue("t1z");
		dMaximalT1z	= getMaxValue("t1z");
		dMinimalT2z	= getMinValue("t2z");
		dMaximalT2z	= getMaxValue("t2z");
		dMinimalT3z	= getMinValue("t3z");
		dMaximalT3z	= getMaxValue("t3z");
		dMinimalZc	= getMinValue("zC");
		dMaximalZc	= getMaxValue("zC");

		DEBUG_STREAM << "#########################################################################" << endl;
		DEBUG_STREAM << "Roll \t--> [" << dMinimalRoll << " ; " << dMaximalRoll << "]" << endl;
		DEBUG_STREAM << "Pitch \t--> [" << dMinimalPitch << " ; " << dMaximalPitch << "]" << endl;
		DEBUG_STREAM << "T1z \t--> [" << dMinimalT1z << " ; " << dMaximalT1z << "]" << endl;
		DEBUG_STREAM << "T2z \t--> [" << dMinimalT2z << " ; " << dMaximalT2z << "]" << endl;
		DEBUG_STREAM << "T3z \t--> [" << dMinimalT3z << " ; " << dMaximalT3z << "]" << endl;
		DEBUG_STREAM << "Zc \t--> [" << dMinimalZc << " ; " << dMaximalZc << "]" << endl;
		DEBUG_STREAM << "#########################################################################" << endl;
	}

	//+------------------------------------------------------------------

	void TraitPointPlan::read5MotorsBounds()
	{
		dMinimalYaw	= getMinValue("yaw");
		dMaximalYaw	= getMaxValue("yaw");
		dMinimalT4x	= getMinValue("t4x");
		dMaximalT4x	= getMaxValue("t4x");
		dMinimalT5x	= getMinValue("t5x");
		dMaximalT5x	= getMaxValue("t5x");
		dMinimalXc	= getMinValue("xC");
		dMaximalXc	= getMaxValue("xC");

		DEBUG_STREAM << "#########################################################################" << endl;
		DEBUG_STREAM << "Yaw \t--> [" << dMinimalYaw << " ; " << dMaximalYaw << "]" << endl;
		DEBUG_STREAM << "T4x \t--> [" << dMinimalT4x << " ; " << dMaximalT4x << "]" << endl;
		DEBUG_STREAM << "T5x \t--> [" << dMinimalT5x << " ; " << dMaximalT5x << "]" << endl;
		DEBUG_STREAM << "Xc \t--> [" << dMinimalXc << " ; " << dMaximalXc << "]" << endl;
		DEBUG_STREAM << "#########################################################################" << endl;

	}
	//+------------------------------------------------------------------

	void TraitPointPlan::readMotorsBounds()
	{
		read3MotorsBounds();
		if (iType == FIVE_MOTORS )
		{
			read5MotorsBounds();
		}
	}
	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::init_device()
	// 
	// description : 	will be called at device initialization.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::init_device()
	{
		INFO_STREAM << "TraitPointPlan::TraitPointPlan() create device " << device_name << endl;

		_bBoundsException = false;
		_sStatusMessage.str("");					//We clean the stringstream
		m_critical_properties_missing = false;
		_bTraitPointPlanInitialized = false;
		_bAllAttributesRead	    = false;

		yat4tango::InnerAppender::initialize(this, 1024);

		// Initialise variables to default values
		//--------------------------------------------
		get_device_property();

		if( m_critical_properties_missing == true )
		{
			set_state( Tango::FAULT );
			_sStatusMessage << "Some properties could not be read or are missing. See expert attribut log for further details" << std::endl;
		}
		///##########################################################################################
		///#####Creation of the attributes
		///##########################################################################################

		CREATE_SCALAR_ATTRIBUTE(attr_roll_read		,0.0);
		CREATE_SCALAR_ATTRIBUTE(attr_pitch_read		,0.0);
		CREATE_SCALAR_ATTRIBUTE(attr_zC_read		,0.0);	
		CREATE_SCALAR_ATTRIBUTE(attr_t1z_read		,0.0);
		CREATE_SCALAR_ATTRIBUTE(attr_t2z_read		,0.0);
		CREATE_SCALAR_ATTRIBUTE(attr_t3z_read		,0.0);
		CREATE_SCALAR_ATTRIBUTE(attr_yaw_read		,0.0);
		CREATE_SCALAR_ATTRIBUTE(attr_xC_read		,0.0);	
		CREATE_SCALAR_ATTRIBUTE(attr_t4x_read		,0.0);
		CREATE_SCALAR_ATTRIBUTE(attr_t5x_read		,0.0);

		//Set the sign factor to 1.0 or -1.0 according the properties "rollDirection" and "pitchDirection"
		if (rollDirection) 
		{
			rollSign = 1.0;
		}
		else
		{
			rollSign = -1.0;
		}
		if (pitchDirection) 
		{
			pitchSign = 1.0;
		}
		else
		{
			pitchSign = -1.0;
		}
		if (yawDirection) 
		{
			yawSign = 1.0;
		}
		else
		{
			yawSign = -1.0;
		}

		if (deviceType == "3")		iType = THREE_MOTORS;
		else if (deviceType == "5") iType = FIVE_MOTORS;
		else if (deviceType == "2") iType = TWO_MOTORS;
		else iType = -1; //Error

		if (iType != -1) 
		{
			readMotorsBounds();

			// if group has not been created , then do it 
			if( m_initialised_group == false )
			{
				init_attr_group();
			}

		}
		else   // if type not correct
		{
			_sStatusMessage		<< "Exception in init_device : " 
				<< "The number of motors must be 3 or 5" << " " 
				<< "INVALID_ARGUMENT" << " " 
				<< "init_device()" << endl;

			_sStatusMessage		<< "\n --> check the deviceType (" << deviceType << ") property !!!" << endl;
		}
	}
	//+------------------------------------------------------------------
	/**
	*	method:	TraitPointPlan::initialize_trait_point_plan
	*
	*	description:	method to execute "InitializeTraitPointPlan"
	*	Method to allow the initialization of the traitpointplan to a reference state.
	*
	*
	*/
	//+------------------------------------------------------------------
	void TraitPointPlan::initialize_trait_point_plan()
	{
		DEBUG_STREAM << "TraitPointPlan::initialize_trait_point_plan(): entering... !" << endl;

		//	Add your own code to control device here

		_sStatusMessage.str("");					//We clean the stringstream
		_bTraitPointPlanInitialized = false;

		_sStatusMessage << "TraitPointPlan Waiting for Initialization\n--> Call the InitializeTraitPointPlan command..." << endl;
		INFO_STREAM << "###################### INITIALIZE TRAITPOINTPLAN #########################" << endl;

		try
		{
			// read motors positions, then compute the corresponding angle values
			// Finally, update the "write" fields of the position attributes and the "read" and "write" fields of the angle attributes
			// Note: The "read" parts of the position attributes are set wheb readig the motors positions.
			update_positions_and_angles();  
			updateAnglesReadAttributes();
			updateAnglesWriteAttributes();
			updateMotorsWriteAttributes();
			_bTraitPointPlanInitialized = true;
			//Manage "old-*" values  are used to store the motors values in order to undo a mouvement.
			old_t1z = t1z;
			old_t2z = t2z;
			if (iType != TWO_MOTORS)
			{
				old_t3z = t3z;
			}

			if (iType == FIVE_MOTORS)
			{
				old_t4x = t4x;
				old_t5x = t5x;	
			}

			INFO_STREAM << "###################### END INITIALIZE TRAITPOINTPLAN #########################" << endl;
		}
		catch (ValueOutOfBoundException& e) 
		{
			ERROR_STREAM << "Exception in initialize_trait_point_plan : " << e.getReason() << " " << e.getDescription() << " " << e.getOrigin() << endl;
			_sStatusMessage << "ERROR : " << e.getReason() << " " << e.getDescription() << " " << e.getOrigin() << endl;
		}
		catch(Tango::DevFailed& e)
		{
			ERROR_STREAM << "Exception in initialize_trait_point_plan : " << endl;//e.reason() << " " << e.getDescription() << " " << e.getOrigin() << endl;	
			_sStatusMessage << "The trait point plan is not fully initialized !!!" << endl;
		}
		catch(...)
		{
			ERROR_STREAM << "Exception in initialize_trait_point_plan : " << endl;//e.reason() << " " << e.getDescription() << " " << e.getOrigin() << endl;
			_sStatusMessage << "The trait point plan is not fully initialized !!!" << endl;
		}
	}

	//+------------------------------------------------------------------

	void TraitPointPlan::CreateAllProperties(Tango::DbData& dev_prop)
	{
		store_value_as_property(dev_prop,"position","AttributePositionName");
		store_value_as_property(dev_prop,"","BackExtElevationMotorName");
		store_value_as_property(dev_prop,"","BackIntElevationMotorName");
		store_value_as_property(dev_prop,"","BackTranslationMotorName");
		store_value_as_property(dev_prop,"","CenterHorizontalToFront");
		store_value_as_property(dev_prop,"","CenterToHeight");
		store_value_as_property(dev_prop,"","CenterVerticalToFront");
		store_value_as_property(dev_prop,"State","CommandStateName");
		store_value_as_property(dev_prop,"Stop","CommandStopName");
		store_value_as_property(dev_prop,"","DeviceType");
		store_value_as_property(dev_prop,"","FrontElevationMotorName");
		store_value_as_property(dev_prop,"","FrontTranslationMotorName");

		store_value_as_property(dev_prop,"","NominalPitch");
		store_value_as_property(dev_prop,"","NominalRoll");
		store_value_as_property(dev_prop,"","NominalXc");
		store_value_as_property(dev_prop,"","NominalYaw");
		store_value_as_property(dev_prop,"","NominalZc");
		store_value_as_property(dev_prop,"true","PitchDirection");
		store_value_as_property(dev_prop,"true","RollDirection");
		store_value_as_property(dev_prop,"true","YawDirection");
		store_value_as_property(dev_prop,"","TableLength");
		store_value_as_property(dev_prop,"","TableWidth");
	}


	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::readDeviceProperies()
	// 
	// description : 	Read the device properties from database.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::get_device_property()
	{
		//	Initialize your default values here.
		//------------------------------------------
		nominalZc								= TPP::NOMINAL_ZC;
		nominalXc								= TPP::NOMINAL_XC;

		nominalRoll								= MAX_PROPERTIES_VALUE;
		nominalPitch							= MAX_PROPERTIES_VALUE;
		nominalYaw								= MAX_PROPERTIES_VALUE;

		tableLength								= TPP::TABLE_LENGTH;
		tableWidth								= TPP::TABLE_WIDTH;

		centerVerticalToFront					= TPP::CENTER_VERTICAL_TO_FRONT;
		centerHorizontalToFront					= TPP::CENTER_HORIZONTAL_TO_FRONT;

		centerToHeight							= TPP::CENTER_TO_HEIGHT;
		commandStateName						= TPP::DEVICE_INIT_PROPERTIES::COMMAND_STATE_NAME;
		rollDirection							= TPP::ROLL_DIRECTION;
		pitchDirection							= TPP::PITCH_DIRECTION;
		yawDirection							= TPP::YAW_DIRECTION;
		commandStopName							= TPP::DEVICE_INIT_PROPERTIES::COMMAND_STOP_NAME;

		dMinimalRoll  = MIN_PROPERTIES_VALUE;
		dMaximalRoll  = MAX_PROPERTIES_VALUE;
		dMinimalPitch = MIN_PROPERTIES_VALUE;
		dMaximalPitch = MAX_PROPERTIES_VALUE;
		dMinimalYaw   = MIN_PROPERTIES_VALUE;
		dMaximalYaw   = MAX_PROPERTIES_VALUE;

		dMinimalT1z								= TPP::MINIMAL_T1Z;
		dMaximalT1z								= TPP::MAXIMAL_T1Z;	
		dMinimalT2z								= TPP::MINIMAL_T2Z;
		dMaximalT2z								= TPP::MAXIMAL_T2Z;
		dMinimalT3z								= TPP::MINIMAL_T3Z;
		dMaximalT3z								= TPP::MAXIMAL_T3Z;	
		dMinimalT4x								= TPP::MINIMAL_T4X;
		dMaximalT4x								= TPP::MAXIMAL_T4X;
		dMinimalT5x								= TPP::MINIMAL_T5X;
		dMaximalT5x								= TPP::MAXIMAL_T5X;

		dMinimalZc								= TPP::MINIMAL_ZC;
		dMaximalZc								= TPP::MAXIMAL_ZC;
		dMinimalXc								= TPP::MINIMAL_XC;
		dMaximalXc								= TPP::MAXIMAL_XC;

		angleUnit                 = TPP::ANGLE_UNIT;

		//	Read device properties from database.(Automatic code generation)
		//-------------------------------------------------------------
		Tango::DbData	dev_prop;
		dev_prop.push_back(Tango::DbDatum("BackExtElevationMotorName"));
		dev_prop.push_back(Tango::DbDatum("BackIntElevationMotorName"));
		dev_prop.push_back(Tango::DbDatum("BackTranslationMotorName"));
		dev_prop.push_back(Tango::DbDatum("CenterHorizontalToFront"));
		dev_prop.push_back(Tango::DbDatum("CenterToHeight"));
		dev_prop.push_back(Tango::DbDatum("CenterVerticalToFront"));
		dev_prop.push_back(Tango::DbDatum("CommandStateName"));
		dev_prop.push_back(Tango::DbDatum("CommandStopName"));
		dev_prop.push_back(Tango::DbDatum("DeviceType"));
		dev_prop.push_back(Tango::DbDatum("FrontElevationMotorName"));
		dev_prop.push_back(Tango::DbDatum("FrontTranslationMotorName"));
		dev_prop.push_back(Tango::DbDatum("NominalPitch"));
		dev_prop.push_back(Tango::DbDatum("NominalRoll"));
		dev_prop.push_back(Tango::DbDatum("NominalXc"));
		dev_prop.push_back(Tango::DbDatum("NominalYaw"));
		dev_prop.push_back(Tango::DbDatum("NominalZc"));
		dev_prop.push_back(Tango::DbDatum("PitchDirection"));
		dev_prop.push_back(Tango::DbDatum("RollDirection"));
		dev_prop.push_back(Tango::DbDatum("YawDirection"));
		dev_prop.push_back(Tango::DbDatum("TableLength"));
		dev_prop.push_back(Tango::DbDatum("TableWidth"));
		dev_prop.push_back(Tango::DbDatum("AngleUnit"));

		//	Call database and extract values
		//--------------------------------------------
		if (Tango::Util::instance()->_UseDb==true)
			get_db_device()->get_property(dev_prop);
		Tango::DbDatum	def_prop, cl_prop;
		TraitPointPlanClass	*ds_class =
			(static_cast<TraitPointPlanClass *>(get_device_class()));
		int	i = -1;

		//	Try to initialize BackExtElevationMotorName from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  backExtElevationMotorName;
		else {
			//	Try to initialize BackExtElevationMotorName from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  backExtElevationMotorName;
		}
		//	And try to extract BackExtElevationMotorName value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  backExtElevationMotorName;

		//	Try to initialize BackIntElevationMotorName from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  backIntElevationMotorName;
		else {
			//	Try to initialize BackIntElevationMotorName from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  backIntElevationMotorName;
		}
		//	And try to extract BackIntElevationMotorName value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  backIntElevationMotorName;

		//	Try to initialize BackTranslationMotorName from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  backTranslationMotorName;
		else {
			//	Try to initialize BackTranslationMotorName from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  backTranslationMotorName;
		}
		//	And try to extract BackTranslationMotorName value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  backTranslationMotorName;

		//	Try to initialize CenterHorizontalToFront from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  centerHorizontalToFront;
		else {
			//	Try to initialize CenterHorizontalToFront from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  centerHorizontalToFront;
		}
		//	And try to extract CenterHorizontalToFront value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  centerHorizontalToFront;

		//	Try to initialize CenterToHeight from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  centerToHeight;
		else {
			//	Try to initialize CenterToHeight from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  centerToHeight;
		}
		//	And try to extract CenterToHeight value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  centerToHeight;

		//	Try to initialize CenterVerticalToFront from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  centerVerticalToFront;
		else {
			//	Try to initialize CenterVerticalToFront from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  centerVerticalToFront;
		}
		//	And try to extract CenterVerticalToFront value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  centerVerticalToFront;

		//	Try to initialize CommandStateName from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  commandStateName;
		else {
			//	Try to initialize CommandStateName from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  commandStateName;
		}
		//	And try to extract CommandStateName value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  commandStateName;

		//	Try to initialize CommandStopName from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  commandStopName;
		else {
			//	Try to initialize CommandStopName from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  commandStopName;
		}
		//	And try to extract CommandStopName value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  commandStopName;

		//	Try to initialize DeviceType from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  deviceType;
		else {
			//	Try to initialize DeviceType from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  deviceType;
		}
		//	And try to extract DeviceType value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  deviceType;

		//	Try to initialize FrontElevationMotorName from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  frontElevationMotorName;
		else {
			//	Try to initialize FrontElevationMotorName from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  frontElevationMotorName;
		}
		//	And try to extract FrontElevationMotorName value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  frontElevationMotorName;

		//	Try to initialize FrontTranslationMotorName from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  frontTranslationMotorName;
		else {
			//	Try to initialize FrontTranslationMotorName from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  frontTranslationMotorName;
		}
		//	And try to extract FrontTranslationMotorName value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  frontTranslationMotorName;

		//	Try to initialize NominalPitch from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  nominalPitch;
		else {
			//	Try to initialize NominalPitch from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  nominalPitch;
		}
		//	And try to extract NominalPitch value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  nominalPitch;

		//	Try to initialize NominalRoll from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  nominalRoll;
		else {
			//	Try to initialize NominalRoll from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  nominalRoll;
		}
		//	And try to extract NominalRoll value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  nominalRoll;

		//	Try to initialize NominalXc from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  nominalXc;
		else {
			//	Try to initialize NominalXc from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  nominalXc;
		}
		//	And try to extract NominalXc value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  nominalXc;

		//	Try to initialize NominalYaw from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  nominalYaw;
		else {
			//	Try to initialize NominalYaw from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  nominalYaw;
		}
		//	And try to extract NominalYaw value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  nominalYaw;

		//	Try to initialize NominalZc from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  nominalZc;
		else {
			//	Try to initialize NominalZc from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  nominalZc;
		}
		//	And try to extract NominalZc value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  nominalZc;

		//	Try to initialize PitchDirection from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  pitchDirection;
		else {
			//	Try to initialize PitchDirection from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  pitchDirection;
		}
		//	And try to extract PitchDirection value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  pitchDirection;

		//	Try to initialize RollDirection from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  rollDirection;
		else {
			//	Try to initialize RollDirection from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  rollDirection;
		}
		//	And try to extract RollDirection value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  rollDirection;

		//	Try to initialize YawDirection from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  yawDirection;
		else {
			//	Try to initialize YawDirection from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  yawDirection;
		}
		//	And try to extract YawDirection value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  yawDirection;

		//	Try to initialize TableLength from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  tableLength;
		else {
			//	Try to initialize TableLength from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  tableLength;
		}
		//	And try to extract TableLength value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  tableLength;

		//	Try to initialize TableWidth from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  tableWidth;
		else {
			//	Try to initialize TableWidth from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  tableWidth;
		}
		//	And try to extract TableWidth value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  tableWidth;

		//	Try to initialize AngleUnit from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  angleUnit;
		else {
			//	Try to initialize AngleUnit from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  angleUnit;
		}
		//	And try to extract AngleUnit value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  angleUnit;



		//	End of Automatic code generation
		//-------------------------------------------------------------

		// Mandatory properties are Proxies on motors

		if( deviceType.empty() == true )
		{
			m_critical_properties_missing = true;
			ERROR_STREAM << "DeviceType is empty . Must be 3 or 5 according to equipment(Check properties)" << std::endl;
		}

		if( frontElevationMotorName.empty() == true )
		{
			m_critical_properties_missing = true;
			ERROR_STREAM << "FrontElevationMotorName is empty (Check properties)" << std::endl;
		}
		if( backExtElevationMotorName.empty() == true )
		{
			m_critical_properties_missing = true;
			ERROR_STREAM << "backExtElevationMotorName is empty (Check properties)" << std::endl;
		}
		if( backIntElevationMotorName.empty() == true &&  deviceType != "2")
		{
			m_critical_properties_missing = true;
			ERROR_STREAM << "backIntElevationMotorName is empty (Check properties)" << std::endl;
		}
		if( ( angleUnit != "Deg" ) && ( angleUnit != "mRad" ) )
		{
			m_critical_properties_missing = true;
			ERROR_STREAM << "angleUnit is invalid (Check properties)" << std::endl;
		}
		else
		{
			m_angle_unit_degree = ( angleUnit == "Deg" );
			DEBUG_STREAM << "angleUnit = " << angleUnit << " m_angle_unit_degree = " << m_angle_unit_degree << std::endl;
		}

		if( deviceType == "5" )
		{
			if( frontTranslationMotorName.empty() == true )
			{
				m_critical_properties_missing = true;
				ERROR_STREAM << "frontTranslationMotorName is empty (Check properties)" << std::endl;
			}
			if( backTranslationMotorName.empty() == true )
			{
				m_critical_properties_missing = true;
				ERROR_STREAM << "backTranslationMotorName is empty (Check properties)" << std::endl;
			}
		}

		// Set uninitialised variables to defaults values (according to angle unit)

		init_if_needed( dMinimalPitch, TPP::MINIMAL_PITCH );
		init_if_needed( dMaximalPitch, TPP::MAXIMAL_PITCH );
		init_if_needed( dMinimalRoll, TPP::MINIMAL_ROLL );
		init_if_needed( dMaximalRoll, TPP::MAXIMAL_ROLL );
		init_if_needed( dMinimalYaw, TPP::MINIMAL_YAW );
		init_if_needed( dMaximalYaw, TPP::MAXIMAL_YAW );

		init_if_needed( nominalPitch, TPP::NOMINAL_PITCH );
		init_if_needed( nominalRoll, TPP::NOMINAL_ROLL );
		init_if_needed( nominalYaw, TPP::NOMINAL_YAW );

		CreateAllProperties(dev_prop);
	}
	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::always_executed_hook()
	// 
	// description : 	method always executed before any command is executed
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::always_executed_hook()
	{
		// Differed initialisation as we must have a device with fully constructed attributes to set write parts
		DEBUG_STREAM << "always_executed_hook entering" << std::endl;

		if (!_bTraitPointPlanInitialized && m_initialised_group == true )
		{
			DEBUG_STREAM << "always_executed_hook : calling initialize_trait_point_plan" << std::endl;  
			// From real motors positions, initialise angles (read and write parts)
			initialize_trait_point_plan();
		}

		DEBUG_STREAM << "always_executed_hook exiting" << std::endl;
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::read_attr_hardware
	// 
	// description : 	Hardware acquisition for attributes.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::read_attr_hardware(vector<long> &attr_list)
	{
		DEBUG_STREAM << "TraitPointPlan::read_attr_hardware(vector<long> &attr_list) entering... "<< endl;

		//	Add your own code here
		if (_bTraitPointPlanInitialized)
		{
			try 
			{
				update_positions_and_angles();
				updateAnglesReadAttributes();
			}		
			catch (ValueOutOfBoundException& e) 
			{
				ERROR_STREAM << "Exception in read_attr_hardware : " << e.getReason() << " " << e.getDescription() << " " << e.getOrigin() << endl;
				_sStatusMessage << "Exception in read_attr_hardware : " << e.getReason() << " " << e.getDescription() << " " << e.getOrigin() << endl;
				_bBoundsException = true;
			}
			catch(Tango::DevFailed& e)
			{	
				ERROR_STREAM << e << endl;
			}
			catch(...)
			{
				ERROR_STREAM << "Unknown Error in read_attr_hardware" << endl;
			}

		}
	}
	//+----------------------------------------------------------------------------
	void TraitPointPlan::updateAllTempValues()
	{
		updateMotorsTempValues();
		updateAnglesTempValues();	
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::write_roll
	// 
	// description : 	Write roll attribute values to hardware.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::write_roll(Tango::WAttribute &attr)
	{
		DEBUG_STREAM << "TraitPointPlan::write_roll(Tango::WAttribute &attr) entering... "<< endl;

		if (iType == TWO_MOTORS)
		{
			Tango::Except::throw_exception 
				(
				"INVALID_DATA",
				" No roll value on 2 axes TraitpointPlan",
				"TraitPointPlan::write_roll"
				);
		}
		// For other TPP types (3 and 5 axes)

		else
		{		
			attr.get_write_value(attr_roll_write);

			try 
			{	
				readMotorsBounds();
				checkRollBounds(attr_roll_write);

				// bug 19055 - Use "write" values instead of "read" for calculations
				pitch	= attr_pitch_write;
				zc	= attr_zC_write;

				if (iType == FIVE_MOTORS)
				{
					xc		= attr_xC_write;
					yaw		= attr_yaw_write;
				}
				// bug 19055 - Use "write" values instead of "read" for calculations

				computeMotorsValues();
				checkMotorsBounds();
				updateMotorsWriteAttributes();
				sendValuesToMotors();
			}		
			catch (Exception& e) 
			{
				ERROR_STREAM << "Exception in write_roll : " << e.getReason() << " " << e.getDescription() << " " << e.getOrigin() << endl;
				Tango::Except::throw_exception 
					(
					(const char *)(e.getReason()).c_str(),
					(const char *)(e.getDescription()).c_str(),
					(const char *)(e.getOrigin()).c_str()
					);
			}
			catch(Tango::DevFailed& e)
			{	
				ERROR_STREAM << e << endl;
				Tango::Except::re_throw_exception (	e,
					(const char *)"Dev_Failed",
					(const char *)"Value roll not good !!!",
					(const char *)"sendValuesToMotors");
			}
			catch(...)
			{
				ERROR_STREAM << "Unknown Error in Exception in write_roll" << endl;
			}
		}
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::computeMotorsValues
	// 
	// description : 	Calculate t1z,t2z,t3z,t4x,t5x motor positions according to roll,pitch ....
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::computeMotorsValues() 
	{	
		double dz;
		double z0;
		double dx;
		double x0;
		double mradRoll;
		double mradPitch;
		double mradYaw;

		if( m_angle_unit_degree == false )
		{
			mradRoll = roll;
			mradPitch = pitch;
			mradYaw = yaw;
		}
		else
		{
			mradRoll = convert_deg_to_mrad( roll );
			mradPitch = convert_deg_to_mrad( pitch );
			mradYaw = convert_deg_to_mrad( yaw );
		}
		if (iType ==  TWO_MOTORS) 
		{
			mradRoll=0;
			mradYaw=0;
		}

		// Then for all cases (2,3 and 5 axes)  do the calculations 
		{
			double transverse_delta = tableWidth/2 * tan(mradRoll/1000)* rollSign;
			double tan_pitch		= tan(mradPitch/1000)* pitchSign;
			double back_delta		= (tableLength - centerVerticalToFront) * tan_pitch;
			double front_delta		= centerVerticalToFront * tan_pitch;

			//Save the current positions to be able to undo the movement
			old_t1z = t1z;
			old_t2z = t2z;
			old_t3z = t3z;

			//To keep the same height at the incident position (along the direction of the beam)
			//we lower the mirror
			dz = centerToHeight * cos(mradRoll/1000)     * cos(mradPitch/1000); 

			z0 = zc - dz;

			t1z = z0 - front_delta;	
			t2z = z0 - transverse_delta + back_delta;
			t3z = z0 + transverse_delta + back_delta;
		}

		// For 2 motors no value for t3z
		if (iType ==  TWO_MOTORS) 
		{
			old_t3z=0;
			t3z=0;
		}

		if (iType ==  FIVE_MOTORS) 
		{
			old_t4x = t4x;
			old_t5x = t5x;

			dx = centerToHeight * sin(mradRoll/1000)  * cos(mradYaw/1000);
			x0 = xc - dx;

			t4x = x0 + yawSign * centerHorizontalToFront*tan(mradYaw/1000);
			t5x = x0 - yawSign * (tableLength - centerHorizontalToFront)*tan(mradYaw/1000);	//"eh3_mirror"
		}
	}


	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::checkMotorsBounds
	// 
	// description : 	Check whether the motors positions are within the system's bounds or not
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::checkMotorsBounds() throw (ValueOutOfBoundException)
	{
		checkT1zBounds(t1z);
		checkT2zBounds(t2z);

		if (iType == THREE_MOTORS || iType == FIVE_MOTORS) 
		{
			checkT3zBounds(t3z);
		}
		if(iType == FIVE_MOTORS)
		{
			checkT4xBounds(t4x);
			checkT5xBounds(t5x);
		}
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::updateMotorsWriteAttributes
	// 
	// description :  set the "write" parts of the position attributes to the internal variable values
	//                Update the Tango attributes too
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::updateMotorsWriteAttributes()
	{
		attr_t1z_write = t1z;
		set_wattribute("t1z",t1z);
		attr_t2z_write = t2z;
		set_wattribute("t2z",t2z);

		if (iType ==  THREE_MOTORS || iType ==  FIVE_MOTORS) 
		{
			attr_t3z_write = t3z;
			set_wattribute("t3z",t3z);
		}

		if (iType ==  FIVE_MOTORS) 
		{
			attr_t4x_write = t4x;
			set_wattribute("t4x",t4x);
			attr_t5x_write = t5x;
			set_wattribute("t5x",t5x);
		}
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::check<attribute>bounds
	// 
	// description :  These methods check if the attribute in the mothod name is within the defined boundaries
	//                An exception is raised if the attribute value is outside the limits
	//
	//-----------------------------------------------------------------------------

	void TraitPointPlan::checkRollBounds(double dRoll)
	{
		if ((dRoll < dMinimalRoll) || (dRoll > dMaximalRoll))
		{
			throw ValueOutOfBoundException("Roll",dRoll,dMinimalRoll,dMaximalRoll,"TraitPointPlan::checkRollBounds()",__FILE__,__LINE__);
		}
		else 
		{
			roll = dRoll;
		}
	}

	void TraitPointPlan::checkPitchBounds(double dPitch)
	{
		if ((dPitch < dMinimalPitch) || (dPitch > dMaximalPitch))
		{
			throw ValueOutOfBoundException("Pitch",dPitch,dMinimalPitch,dMaximalPitch,"TraitPointPlan::checkPitchBounds()",__FILE__,__LINE__);
		}
		else 
		{
			pitch = dPitch;
		}
	}

	void TraitPointPlan::checkYawBounds(double dYaw)
	{
		if ((dYaw < dMinimalYaw) || (dYaw > dMaximalYaw))
		{
			throw ValueOutOfBoundException("Yaw",dYaw,dMinimalYaw,dMaximalYaw,"TraitPointPlan::checkYawBounds()",__FILE__,__LINE__);
		}
		else 
		{
			yaw = dYaw;
		}
	}

	void TraitPointPlan::checkT1zBounds(double dT1z)
	{
		if ((dT1z < dMinimalT1z) || (dT1z > dMaximalT1z))
		{
			throw ValueOutOfBoundException("T1z",dT1z,dMinimalT1z,dMaximalT1z,"TraitPointPlan::checkT1zBounds()",__FILE__,__LINE__);
		}
		else 
		{
			t1z = dT1z;
		}
	}

	void TraitPointPlan::checkT2zBounds(double dT2z)
	{
		if ((dT2z < dMinimalT2z) || (dT2z > dMaximalT2z))
		{
			throw ValueOutOfBoundException("T2z",dT2z,dMinimalT2z,dMaximalT2z,"TraitPointPlan::checkT2zBounds()",__FILE__,__LINE__);
		}
		else 
		{
			t2z = dT2z;
		}
	}

	void TraitPointPlan::checkT3zBounds(double dT3z)
	{
		if ((dT3z < dMinimalT3z) || (dT3z > dMaximalT3z))
		{
			throw ValueOutOfBoundException("T3z",dT3z,dMinimalT3z,dMaximalT3z,"TraitPointPlan::checkT3zBounds()",__FILE__,__LINE__);
		}
		else 
		{
			t3z = dT3z;
		}
	}

	void TraitPointPlan::checkT4xBounds(double dT4x)
	{
		if ((dT4x < dMinimalT4x) || (dT4x > dMaximalT4x))
		{
			throw ValueOutOfBoundException("T4x",dT4x,dMinimalT4x,dMaximalT4x,"TraitPointPlan::checkT4xBounds()",__FILE__,__LINE__);
		}
		else 
		{
			t4x = dT4x;
		}
	}

	void TraitPointPlan::checkT5xBounds(double dT5x)
	{
		if ((dT5x < dMinimalT5x) || (dT5x > dMaximalT5x))
		{
			throw ValueOutOfBoundException("T5x",dT5x,dMinimalT5x,dMaximalT5x,"TraitPointPlan::checkT5xBounds()",__FILE__,__LINE__);
		}
		else 
		{
			t5x = dT5x;
		}
	}

	void TraitPointPlan::checkZcBounds(double dZc)
	{
		double dZcTemp = dZc - centerToHeight;

		if ((dZcTemp < dMinimalZc) || (dZcTemp > dMaximalZc))
		{
			throw ValueOutOfBoundException("Zc",dZcTemp,dMinimalZc,dMaximalZc,"TraitPointPlan::checkZcBounds()",__FILE__,__LINE__);
		}
		else 
		{
			zc = dZc;
		}
	}

	void TraitPointPlan::checkXcBounds(double dXc)
	{
		if ((dXc < dMinimalXc) || (dXc > dMaximalXc))
		{
			throw ValueOutOfBoundException("Xc",dXc,dMinimalXc,dMaximalXc,"TraitPointPlan::checkXcBounds()",__FILE__,__LINE__);
		}
		else 
		{
			xc = dXc;
		}
	}


	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::write_pitch
	// 
	// description : 	Write pitch attribute values to hardware.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::write_pitch(Tango::WAttribute &attr)
	{
		DEBUG_STREAM << "TraitPointPlan::write_pitch(Tango::WAttribute &attr) entering... "<< endl;

		try 
		{	
			attr.get_write_value(attr_pitch_write);
			readMotorsBounds();
			checkPitchBounds(attr_pitch_write);

			// bug 19055 - Use "write" values instead of "read" for calculations
			if (iType == TWO_MOTORS)
				roll =0;
			else
				roll	= attr_roll_write;

			// for all cases 
			zc		= attr_zC_write;

			// for 5 axes 
			if (iType == FIVE_MOTORS)
			{
				xc		= attr_xC_write;
				yaw		= attr_yaw_write;
			}
			// bug 19055 - Use "write" values instead of "read" for calculations

			computeMotorsValues();
			checkMotorsBounds();
			updateMotorsWriteAttributes();
			sendValuesToMotors();
		}		
		catch (Exception& e) 
		{
			ERROR_STREAM << "Exception in write_pitch : " << e.getReason() << " " << e.getDescription() << " " << e.getOrigin() << endl;
			Tango::Except::throw_exception 
				(
				(const char *)(e.getReason()).c_str(),
				(const char *)(e.getDescription()).c_str(),
				(const char *)(e.getOrigin()).c_str()
				);
		}
		catch(Tango::DevFailed& e)
		{	
			ERROR_STREAM << "Exception Dev_Failed in write_pitch : " << endl;
			Tango::Except::re_throw_exception (	e,
				(const char *)"Dev_Failed",
				(const char *)"Value pitch not good !!!",
				(const char *)"sendValuesToMotors");
		}
		catch(...)
		{
			ERROR_STREAM << "Unknown Error in write_pitch : " << endl;
		}
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::write_yaw
	// 
	// description : 	Write yaw attribute values to hardware.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::write_yaw(Tango::WAttribute &attr)
	{
		DEBUG_STREAM << "TraitPointPlan::write_yaw(Tango::WAttribute &attr) entering... "<< endl;

		if (iType == FIVE_MOTORS) 
		{
			try 
			{	
				attr.get_write_value(attr_yaw_write);
				readMotorsBounds();
				checkYawBounds(attr_yaw_write);

				// bug 19055 - Use "write" values instead of "read" for calculations
				roll	= attr_roll_write;
				pitch	= attr_pitch_write;
				zc	= attr_zC_write;
				xc = attr_xC_write;

				computeMotorsValues();
				checkMotorsBounds();
				updateMotorsWriteAttributes();
				sendValuesToMotors();
			}		
			catch (Exception& e) 
			{
				ERROR_STREAM << "Exception in write_yaw : " << e.getReason() << " " << e.getDescription() << " " << e.getOrigin() << endl;
				Tango::Except::throw_exception 
					(
					(const char *)(e.getReason()).c_str(),
					(const char *)(e.getDescription()).c_str(),
					(const char *)(e.getOrigin()).c_str()
					);
			}
			catch(Tango::DevFailed& e)
			{	
				ERROR_STREAM << e << endl;
				Tango::Except::re_throw_exception (	e,
					(const char *)"Dev_Failed",
					(const char *)"Value yaw not good !!!",
					(const char *)"sendValuesToMotors");
			}
			catch(...)
			{
				ERROR_STREAM << "Unknown Error in write_yaw " << endl;
			}
		}
		else
		{
			attr.set_quality(Tango::ATTR_INVALID);
			Tango::Except::throw_exception 
				(
				"INVALID_DATA",
				" No yaw value on 2 or 3 axes TraitpointPlan",
				"TraitPointPlan::write_roll"
				);
		}
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::write_zC
	// 
	// description : 	Write zC attribute values to hardware.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::write_zC(Tango::WAttribute &attr)
	{
		DEBUG_STREAM << "TraitPointPlan::write_zC(Tango::WAttribute &attr) entering... "<< endl;

		attr.get_write_value(attr_zC_write);
		attr_zC_write += centerToHeight;

		try 
		{	
			readMotorsBounds();
			checkZcBounds(attr_zC_write );

			// bug 19055 - Use "write" values instead of "read" for calculations

			if (iType == TWO_MOTORS)
				roll =0;
			else
				roll	= attr_roll_write;

			// for all cases
			pitch	= attr_pitch_write;

			if (iType == FIVE_MOTORS)
			{
				yaw		= attr_yaw_write;
				xc		= attr_xC_write;
			}
			// bug 19055 - Use "write" values instead of "read" for calculations

			computeMotorsValues();	
			checkMotorsBounds();
			updateMotorsWriteAttributes();
			sendValuesToMotors();
		}		
		catch (Exception& e) 
		{
			ERROR_STREAM << "Exception in write_zC : " << e.getReason() << " " << e.getDescription() << " " << e.getOrigin() << endl;
			Tango::Except::throw_exception 
				(
				(const char *)(e.getReason()).c_str(),
				(const char *)(e.getDescription()).c_str(),
				(const char *)(e.getOrigin()).c_str()
				);
		}
		catch(Tango::DevFailed& e)
		{	
			ERROR_STREAM << e << endl;
			Tango::Except::re_throw_exception (	e,
				(const char *)"Dev_Failed",
				(const char *)"Value zC not good !!!",
				(const char *)"sendValuesToMotors");
		}
		catch(...)
		{
			ERROR_STREAM << "Unknown Error in write_zC " << endl;
		}
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::write_xC
	// 
	// description : 	Write xC attribute values to hardware.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::write_xC(Tango::WAttribute &attr)
	{
		DEBUG_STREAM << "TraitPointPlan::write_xC(Tango::WAttribute &attr) entering... "<< endl;

		if (iType == FIVE_MOTORS) 
		{
			attr.get_write_value(attr_xC_write);

			try 
			{	
				readMotorsBounds();
				checkXcBounds(attr_xC_write);

				// bug 19055 - Use "write" values instead of "read" for calculations
				roll	= attr_roll_write;
				pitch	= attr_pitch_write;
				zc		= attr_zC_write;
				yaw		= attr_yaw_write;
				// bug 19055 - Use "write" values instead of "read" for calculations

				computeMotorsValues();
				checkMotorsBounds();
				updateMotorsWriteAttributes();
				sendValuesToMotors();
			}		
			catch (Exception& e) 
			{
				ERROR_STREAM << "Exception in write_xC : " << e.getReason() << " " << e.getDescription() << " " << e.getOrigin() << endl;
				Tango::Except::throw_exception 
					(
					(const char *)(e.getReason()).c_str(),
					(const char *)(e.getDescription()).c_str(),
					(const char *)(e.getOrigin()).c_str()
					);
			}
			catch(Tango::DevFailed& e)
			{	
				ERROR_STREAM << e << endl;
				Tango::Except::re_throw_exception (	e,
					(const char *)"Dev_Failed",
					(const char *)"Value xC not good !!!",
					(const char *)"sendValuesToMotors");
			}
			catch(...)
			{
				INFO_STREAM << "Unknown Error in write_xC" << endl;
			}
		}
		else
		{
			Tango::Except::throw_exception 
				(
				"INVALID_DATA",
				" No xC value on 2 or 3 axes TraitpointPlan",
				"TraitPointPlan::write_roll"
				);
			attr.set_quality(Tango::ATTR_INVALID);
		}
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::write_t1z
	// 
	// description : 	Write t1z attribute values to hardware.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::write_t1z(Tango::WAttribute &attr)
	{
		DEBUG_STREAM << "TraitPointPlan::write_t1z(Tango::WAttribute &attr) entering... "<< endl;
		attr.get_write_value(attr_t1z_write);

		//Thanks to the values of the motors, the roll, pitch, yaw, Zc, Xc are computed.
		try 
		{	
			readMotorsBounds();
			checkT1zBounds(attr_t1z_write);

			// bug 19055 - Use "write" values instead of "read" for calculations=
			t2z		= attr_t2z_write;
			if (iType == TWO_MOTORS)
				t3z = 0;
			else
				t3z	= attr_t3z_write;

			if (iType == FIVE_MOTORS)
			{
				t4x		= attr_t4x_write;
				t5x		= attr_t5x_write;
			}
			// bug 19055 - Use "write" values instead of "read" for calculations=

			computeAngles();
			checkAnglesBounds();
			updateAnglesWriteAttributes();

			// having reached this stage in the process means there were no previous errors
			// so it is safe to set t1 to its "write" value
			t1z = attr_t1z_write;
			sendValuesToMotors();
		}		
		catch (Exception& e) 
		{
			ERROR_STREAM << "Exception in write_t1z : " << e.getReason() << " " << e.getDescription() << " " << e.getOrigin() << endl;
			Tango::Except::throw_exception 
				(
				(const char *)(e.getReason()).c_str(),
				(const char *)(e.getDescription()).c_str(),
				(const char *)(e.getOrigin()).c_str()
				);
		}
		catch(Tango::DevFailed& e)
		{	
			ERROR_STREAM << e << endl;
			Tango::Except::re_throw_exception (	e,
				(const char *)"Dev_Failed",
				(const char *)"Unable to write T1z motor",
				(const char *)"sendValuesToMotors");
		}
		catch(...)
		{
			ERROR_STREAM << "Unknown Error in write_t1z" << endl;
		}
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::write_t2z
	// 
	// description : 	Write t2z attribute values to hardware.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::write_t2z(Tango::WAttribute &attr)
	{
		DEBUG_STREAM << "TraitPointPlan::write_t2z(Tango::WAttribute &attr) entering... "<< endl;
		attr.get_write_value(attr_t2z_write);

		try 
		{	
			readMotorsBounds();
			checkT2zBounds(attr_t2z_write);

			// bug 19055 - Use "write" values instead of "read" for calculations=
			t1z		= attr_t1z_write;
			if (iType == TWO_MOTORS)
				t3z = 0;
			else
				t3z	= attr_t3z_write;

			if (iType == FIVE_MOTORS)
			{
				t4x		= attr_t4x_write;
				t5x		= attr_t5x_write;
			}
			// bug 19055 - Use "write" values instead of "read" for calculations=

			computeAngles();
			checkAnglesBounds();
			updateAnglesWriteAttributes();

			t2z	= attr_t2z_write;
			sendValuesToMotors();

		}

		catch (Exception& e) 
		{
			ERROR_STREAM << "Exception in write_t2z : " << e.getReason() << " " << e.getDescription() << " " << e.getOrigin() << endl;
			Tango::Except::throw_exception 
				(
				(const char *)(e.getReason()).c_str(),
				(const char *)(e.getDescription()).c_str(),
				(const char *)(e.getOrigin()).c_str()
				);
		}
		catch(Tango::DevFailed& e)
		{	
			INFO_STREAM << "Exception Dev_Failed in write_t2z : " << endl;
			Tango::Except::re_throw_exception (	e,
				(const char *)"Dev_Failed",
				(const char *)"Unable to write T2z motor",
				(const char *)"sendValuesToMotors");
		}
		catch(...)
		{
			INFO_STREAM << "Unknown Error in write_t2z : " << endl;
		}
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::write_t3z
	// 
	// description : 	Write t3z attribute values to hardware.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::write_t3z(Tango::WAttribute &attr)
	{

		if (iType == TWO_MOTORS)
		{
			Tango::Except::throw_exception 
				(
				"INVALID_DATA",
				" No t3z value on 2 axes TraitPointPlan",
				"TraitPointPlan::write_t3z"
				);
		}
		// For other TPP types (3 and 5 axes)

		else
		{		
			DEBUG_STREAM << "TraitPointPlan::write_t3z(Tango::WAttribute &attr) entering... "<< endl;
			attr.get_write_value(attr_t3z_write);

			try 
			{	
				readMotorsBounds();
				checkT3zBounds(attr_t3z_write);

				// bug 19055 - Use "write" values instead of "read" for calculations=
				t1z		= attr_t1z_write;
				t2z		= attr_t2z_write;

				if (iType == FIVE_MOTORS)
				{
					t4x		= attr_t4x_write;
					t5x		= attr_t5x_write;
				}
				// bug 19055 - Use "write" values instead of "read" for calculations=

				computeAngles();
				checkAnglesBounds();
				updateAnglesWriteAttributes();

				t3z	= attr_t3z_write;
				sendValuesToMotors();
			}		
			catch (Exception& e) 
			{
				ERROR_STREAM << "Exception in write_t3z : " << e.getReason() << " " << e.getDescription() << " " << e.getOrigin() << endl;
				Tango::Except::throw_exception 
					(
					(const char *)(e.getReason()).c_str(),
					(const char *)(e.getDescription()).c_str(),
					(const char *)(e.getOrigin()).c_str()
					);
			}
			catch(Tango::DevFailed& e)
			{	
				ERROR_STREAM << e << endl;
				Tango::Except::re_throw_exception (	e,
					(const char *)"Dev_Failed",
					(const char *)"Unable to write T3z motor",
					(const char *)"sendValuesToMotors");
			}
			catch(...)
			{
				ERROR_STREAM << "Unknown Error in write_t3z" << endl;
			}
		}
	}
	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::write_t4x
	// 
	// description : 	Write t4x attribute values to hardware.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::write_t4x(Tango::WAttribute &attr)
	{
		DEBUG_STREAM << "TraitPointPlan::write_t4x(Tango::WAttribute &attr) entering... "<< endl;
		if (iType == FIVE_MOTORS) 
		{
			attr.get_write_value(attr_t4x_write);

			try 
			{	
				readMotorsBounds();
				checkT4xBounds(attr_t4x_write);

				// bug 19055 - Use "write" values instead of "read" for calculations=
				t1z		= attr_t1z_write;
				t2z		= attr_t2z_write;
				t3z		= attr_t3z_write;
				t5x		= attr_t5x_write;
				// bug 19055 - Use "write" values instead of "read" for calculations=

				computeAngles();
				checkAnglesBounds();
				updateAnglesWriteAttributes();

				t4x = attr_t4x_write;
				sendValuesToMotors();

			}		
			catch (Exception& e) 
			{
				ERROR_STREAM << "Exception in write_t4x : " << e.getReason() << " " << e.getDescription() << " " << e.getOrigin() << endl;
				Tango::Except::throw_exception 
					(
					(const char *)(e.getReason()).c_str(),
					(const char *)(e.getDescription()).c_str(),
					(const char *)(e.getOrigin()).c_str()
					);
			}
			catch(Tango::DevFailed& e)
			{	
				ERROR_STREAM << e << endl;
				Tango::Except::re_throw_exception (	e,
					(const char *)"Dev_Failed",
					(const char *)"Unable to write T4x motor",
					(const char *)"sendValuesToMotors");
			}
			catch(...)
			{
				ERROR_STREAM << "Unknown Error in write_t4x" << endl;
			}
		}
		else
		{
			attr.set_quality(Tango::ATTR_INVALID);
			Tango::Except::throw_exception 
				(
				"INVALID_DATA",
				" No t4x value on 2 axes TraitpointPlan",
				"TraitPointPlan::write_roll"
				);
		}
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::write_t5x
	// 
	// description : 	Write t5x attribute values to hardware.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::write_t5x(Tango::WAttribute &attr)
	{
		DEBUG_STREAM << "TraitPointPlan::write_t5x(Tango::WAttribute &attr) entering... "<< endl;
		if (iType == FIVE_MOTORS) 
		{
			attr.get_write_value(attr_t5x_write);

			try 
			{	
				readMotorsBounds();
				checkT5xBounds(attr_t5x_write);

				// bug 19055 - Use "write" values instead of "read" for calculations=
				t1z		= attr_t1z_write;
				t2z		= attr_t2z_write;
				t3z		= attr_t3z_write;
				t4x		= attr_t4x_write;
				// bug 19055 - Use "write" values instead of "read" for calculations=

				computeAngles();
				checkAnglesBounds();
				updateAnglesWriteAttributes();

				t5x = attr_t5x_write;
				sendValuesToMotors();

			}
			catch (Exception& e) 
			{
				ERROR_STREAM << "Exception in write_t5x : " << e.getReason() << " " << e.getDescription() << " " << e.getOrigin() << endl;
				Tango::Except::throw_exception 	(
					(const char *)(e.getReason()).c_str(),
					(const char *)(e.getDescription()).c_str(),
					(const char *)(e.getOrigin()).c_str()
					);
			}
			catch(Tango::DevFailed& e)
			{	
				ERROR_STREAM << e << endl;
				Tango::Except::re_throw_exception (	e,
					(const char *)"Dev_Failed",
					(const char *)"Unable to write T5x motor",
					(const char *)"sendValuesToMotors");
			}
			catch(...)
			{
				ERROR_STREAM << "Unknown Error in write_t5x " << endl;
			}
		}
		else
		{
			attr.set_quality(Tango::ATTR_INVALID);
			Tango::Except::throw_exception 
				(
				"INVALID_DATA",
				" No t4x value on 2 axes TraitpointPlan",
				"TraitPointPlan::write_roll"
				);
		}

	}


	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::read_roll
	// 
	// description : 	Extract real attribute values for roll acquisition result.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::read_roll(Tango::Attribute &attr)
	{
		DEBUG_STREAM << "TraitPointPlan::read_roll(Tango::Attribute &attr) entering... "<< endl;
		if (iType == THREE_MOTORS || iType == FIVE_MOTORS ) 
		{
			roll = *attr_roll_read;
			attr.set_value(attr_roll_read);	
		}
		else
			attr.set_quality(Tango::ATTR_INVALID);
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::read_pitch
	// 
	// description : 	Extract real attribute values for pitch acquisition result.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::read_pitch(Tango::Attribute &attr)
	{
		DEBUG_STREAM << "TraitPointPlan::read_pitch(Tango::Attribute &attr) entering... "<< endl;

		pitch = *attr_pitch_read;
		attr.set_value(attr_pitch_read);
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::read_yaw
	// 
	// description : 	Extract real attribute values for yaw acquisition result.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::read_yaw(Tango::Attribute &attr)
	{
		DEBUG_STREAM << "TraitPointPlan::read_yaw(Tango::Attribute &attr) entering... "<< endl;
		if (iType == FIVE_MOTORS) 
		{
			attr.set_value(attr_yaw_read);
			yaw = *attr_yaw_read;
		}
		else
			attr.set_quality(Tango::ATTR_INVALID);
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::read_zC
	// 
	// description : 	Extract real attribute values for zC acquisition result.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::read_zC(Tango::Attribute &attr)
	{
		DEBUG_STREAM << "TraitPointPlan::read_zC(Tango::Attribute &attr) entering... "<< endl;

		*attr_zC_read = zc;// - centerToHeight;
		zcValTemp = *attr_zC_read - centerToHeight;
		attr.set_value(&zcValTemp);
	}


	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::read_xC
	// 
	// description : 	Extract real attribute values for xC acquisition result.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::read_xC(Tango::Attribute &attr)
	{
		DEBUG_STREAM << "TraitPointPlan::read_xC(Tango::Attribute &attr) entering... "<< endl;

		if (iType == FIVE_MOTORS) 
		{
			xc = *attr_xC_read;
			attr.set_value(attr_xC_read);
		}
		else
			attr.set_quality(Tango::ATTR_INVALID);
	}


	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::sendValuesToMotors()
	// 
	// description : 	Write the calculated value to the motors.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::sendValuesToMotors()  throw (Tango::DevFailed)

	{
		std::vector<double> w_values_vect;

		w_values_vect.clear();

		w_values_vect.push_back( t1z );
		w_values_vect.push_back( t2z );

		if (iType == THREE_MOTORS || iType == FIVE_MOTORS)
		{
			w_values_vect.push_back( t3z );
		}
		if (iType == FIVE_MOTORS)
		{
			w_values_vect.push_back( t4x );
			w_values_vect.push_back( t5x );
		}

		// If writing on attributegroup fails exception has to be caught by caller 
		tpp_attr_grp.write( w_values_vect );

	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::readAllMotors()
	// 
	// description : 	Read the position of the motors.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::readAllMotors() 
	{
		DEBUG_STREAM << "TraitPointPlan::readAllMotors entering... "<< endl;

		std::vector<double> positions;
		_bAllAttributesRead =true;
		yat4tango::AttributeGroupReplyList group_reply_list;

		try
		{
			group_reply_list = tpp_attr_grp.read();
		}
		catch( Tango::DevFailed &df )
		{
			ERROR_STREAM << df << endl;
			set_state(Tango::FAULT);
			_sStatusMessage << "Device is in FAULT state: Read attribute group failed (see log for details )" << std::endl;
			_bAllAttributesRead = false;
		}
		catch(...)
		{
			set_state(Tango::FAULT);
			_sStatusMessage << "Device is in FAULT state: Read attribute group failed" << std::endl;
			_bAllAttributesRead = false;
		}

		size_t attr_ind = 0;

		if( ( _bAllAttributesRead == true ) && (group_reply_list[attr_ind].has_failed() == false ) )
		{
			try
			{
				group_reply_list[attr_ind].get_data() >> positions;
				*attr_t1z_read = positions[0];
			}
			catch(...)
			{
				set_state( Tango::FAULT );
				_sStatusMessage << "Attribute read failed for motor T1z" << std::endl;
				_bAllAttributesRead = false;
			}
		}
		else
		{
			set_state( Tango::FAULT );
			_sStatusMessage << "Attribute read failed for motor T1z" << std::endl;
			_bAllAttributesRead = false;
		}
		attr_ind++;

		if( ( _bAllAttributesRead == true ) && (group_reply_list[attr_ind].has_failed() == false ) )
		{
			try
			{
				group_reply_list[attr_ind].get_data() >> positions;
				*attr_t2z_read = positions[0];
			}
			catch(...)
			{
				set_state( Tango::FAULT );
				_sStatusMessage << "Attribute read failed for motor T2z" << std::endl;
				_bAllAttributesRead = false;
			}
		}
		else
		{
			set_state( Tango::FAULT );
			_sStatusMessage << "Attribute read failed for motor T2z" << std::endl;
			_bAllAttributesRead = false;
		}
		attr_ind++;

		// T3 is only for 3 and 5 axes TPP
		if (iType == THREE_MOTORS || iType == FIVE_MOTORS)
		{
			if( ( _bAllAttributesRead == true ) && (group_reply_list[attr_ind].has_failed() == false ) )
			{
				try
				{
					group_reply_list[attr_ind].get_data() >> positions;
					*attr_t3z_read = positions[0];
				}
				catch(...)
				{
					set_state( Tango::FAULT );
					_sStatusMessage << "Attribute read failed for motor T3z" << std::endl;
					_bAllAttributesRead = false;
				}
			}
			else
			{
				set_state( Tango::FAULT );
				_sStatusMessage << "Attribute read failed for motor T3z" << std::endl;
				_bAllAttributesRead = false;
			}
			attr_ind++;
		}

		if (iType == FIVE_MOTORS)
		{
			if( ( _bAllAttributesRead == true ) && (group_reply_list[attr_ind].has_failed() == false ) )
			{
				try
				{
					group_reply_list[attr_ind].get_data() >> positions;
					*attr_t4x_read = positions[0];
				}
				catch(...)
				{
					set_state( Tango::FAULT );
					_sStatusMessage << "Attribute read failed for motor T4x" << std::endl;
					_bAllAttributesRead = false;
				}
			}
			else
			{
				set_state( Tango::FAULT );
				_sStatusMessage << "Attribute read failed for motor T4x" << std::endl;
				_bAllAttributesRead = false;
			}
			attr_ind++;

			if( ( _bAllAttributesRead == true ) && (group_reply_list[attr_ind].has_failed() == false ) )
			{
				try
				{
					group_reply_list[attr_ind].get_data() >> positions;
					*attr_t5x_read = positions[0];
				}
				catch(...)
				{
					set_state( Tango::FAULT );
					_sStatusMessage << "Attribute read failed for motor T5x" << std::endl;
					_bAllAttributesRead = false;
				}
			}
			else
			{
				set_state( Tango::FAULT );
				_sStatusMessage << "Attribute read failed for motor T5x" << std::endl;
				_bAllAttributesRead = false;
			}
		}
		DEBUG_STREAM << "TraitPointPlan::readAllMotors *attr_t1z_read =  " << *attr_t1z_read  << endl;
		DEBUG_STREAM << "TraitPointPlan::readAllMotors *attr_t2z_read =  " << *attr_t2z_read  << endl;
		DEBUG_STREAM << "TraitPointPlan::readAllMotors *attr_t3z_read =  " << *attr_t3z_read  << endl;
		DEBUG_STREAM << "TraitPointPlan::readAllMotors *attr_t4x_read =  " << *attr_t4x_read  << endl;
		DEBUG_STREAM << "TraitPointPlan::readAllMotors *attr_t5x_read =  " << *attr_t5x_read  << endl;

		DEBUG_STREAM << "TraitPointPlan::readAllMotors exiting... "<< endl;
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::updateMotorsTempValues()
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::updateMotorsTempValues()
	{
		t1z = *attr_t1z_read;
		t2z = *attr_t2z_read;
		t3z = *attr_t3z_read;

		if (iType ==  TWO_MOTORS ) 
		{
			t3z=0;
		}

		if (iType == FIVE_MOTORS) 
		{
			t4x = *attr_t4x_read;
			t5x = *attr_t5x_read;
		}
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::updateAnglesTempValues()
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::updateAnglesTempValues()
	{
		pitch	= *attr_pitch_read;
		zc	= *attr_zC_read;
		roll	= *attr_roll_read;

		if (iType ==  TWO_MOTORS ) 
		{
			roll=0;
		}		

		if (iType == FIVE_MOTORS) 
		{
			yaw = *attr_yaw_read;
			xc = *attr_xC_read;
		}
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::read_t1z
	// 
	// description : 	Extract real attribute values for t1z acquisition result.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::read_t1z(Tango::Attribute &attr)
	{
		DEBUG_STREAM << "TraitPointPlan::read_t1z(Tango::Attribute &attr) entering... "<< endl;
		attr.set_value(attr_t1z_read);
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::read_t2z
	// 
	// description : 	Extract real attribute values for t2z acquisition result.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::read_t2z(Tango::Attribute &attr)
	{
		DEBUG_STREAM << "TraitPointPlan::read_t2z(Tango::Attribute &attr) entering... "<< endl;	
		attr.set_value(attr_t2z_read);
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::read_t3z
	// 
	// description : 	Extract real attribute values for t3z acquisition result.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::read_t3z(Tango::Attribute &attr)
	{
		DEBUG_STREAM << "TraitPointPlan::read_t3z(Tango::Attribute &attr) entering... "<< endl;
		if (iType ==  THREE_MOTORS  || iType ==  FIVE_MOTORS) 
		{
			attr.set_value(attr_t3z_read);
		}
		else
			attr.set_quality(Tango::ATTR_INVALID);

	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::read_t4x
	// 
	// description : 	Extract real attribute values for t4x acquisition result.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::read_t4x(Tango::Attribute &attr)
	{
		DEBUG_STREAM << "TraitPointPlan::read_t4x(Tango::Attribute &attr) entering... "<< endl;
		if (iType == FIVE_MOTORS) 
		{
			attr.set_value(attr_t4x_read);
		}
		else
			attr.set_quality(Tango::ATTR_INVALID);
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::read_t5x
	// 
	// description : 	Extract real attribute values for t5x acquisition result.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::read_t5x(Tango::Attribute &attr)
	{
		DEBUG_STREAM << "TraitPointPlan::read_t5x(Tango::Attribute &attr) entering... "<< endl;
		if (iType == FIVE_MOTORS) 
		{
			attr.set_value(attr_t5x_read);
		}
		else
			attr.set_quality(Tango::ATTR_INVALID);
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::calculatingAngles
	// 
	// description : 	Calculate the roll, pitch , yaw, Zc and Xc from motor positions.
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::computeAngles() 
	{
		DEBUG_STREAM << "TraitPointPlan::computeAngles entering... "<< endl;

		double z0;
		double dz;
		double x0;
		double dx;
		double mradRoll;
		double mradPitch;
		double mradYaw;

		if (tableWidth == 0) 
			throw DivideByZeroException("1000*atan((t3z - t2z)/ tableWidth)","tableWidth","void TraitPointPlan::computeAngles()",__FILE__,__LINE__);
		if (tableLength == 0) 
			throw DivideByZeroException("1000*atan(((t3z + t2z)/2 - t1z)/ tableLength)","tableLength","void TraitPointPlan::computeAngles()",__FILE__,__LINE__);

		if (iType == TWO_MOTORS) // Two motors
		{
			mradRoll	= 0;
			mradPitch	= pitchSign*(THOUSAND*atan( (t2z - t1z)/ tableLength) );

			z0		= t1z + ( t2z - t1z)*centerVerticalToFront/tableLength;
			dz		= centerToHeight  * cos(mradPitch/THOUSAND);
			zc		= z0 + dz;
		}

		// 3 and 5 axes 
		if (iType == THREE_MOTORS || iType == FIVE_MOTORS ) // Two motors
		{

			// calculate the positions. the movement being always positive when
			// going outside the beam and negative going toward the beam,
			mradRoll	= rollSign*(THOUSAND*atan((t3z - t2z)/ tableWidth));
			mradPitch	= pitchSign*(THOUSAND*atan(((t3z + t2z)/2 - t1z)/ tableLength));

			z0		= t1z + ((t3z + t2z)/2 - t1z)*centerVerticalToFront/tableLength;
			dz		= centerToHeight * cos(mradRoll/THOUSAND) * cos(mradPitch/THOUSAND);
			zc		= z0 + dz;
		}


		if (iType == FIVE_MOTORS) //Five motors
		{
			mradYaw	= yawSign*THOUSAND*atan((t4x - t5x)/tableLength);

			x0			= t4x + (t5x - t4x)*centerHorizontalToFront/tableLength;		//"eh3 mirror"
			dx			= centerToHeight * sin(mradRoll/THOUSAND) * cos(mradYaw/THOUSAND);
			xc			= x0 + dx;

			if( m_angle_unit_degree == false )
			{
				yaw = mradYaw;
			}
			else
			{
				yaw = convert_mrad_to_deg( mradYaw );
			}

		}

		if( m_angle_unit_degree == false )
		{
			roll = mradRoll;
			pitch = mradPitch;
		}
		else
		{
			roll = convert_mrad_to_deg( mradRoll );
			pitch = convert_mrad_to_deg( mradPitch );
		}
		DEBUG_STREAM << "TraitPointPlan::computeAngles exiting... "<< endl;
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::checkAnglesBounds
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::checkAnglesBounds() throw (ValueOutOfBoundException)
	{
		checkPitchBounds(pitch);
		checkZcBounds(zc);

		if (iType ==  THREE_MOTORS || iType ==  FIVE_MOTORS) 
		{
			checkRollBounds(roll);
		}
		if(iType == FIVE_MOTORS)
		{
			checkYawBounds(yaw);
			checkXcBounds(xc);
		}
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::updateAnglesAttributes
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::updateAnglesReadAttributes()
	{
		//If there is no exception launched we can update the values.
		*attr_pitch_read	= pitch;
		*attr_zC_read		= zc;
		*attr_roll_read		= roll;


		if (iType ==  TWO_MOTORS) 
			*attr_roll_read =0;

		if (iType == FIVE_MOTORS) //Five motors
		{
			*attr_yaw_read	= yaw;
			*attr_xC_read	= xc;
		}
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		TraitPointPlan::updateAnglesWriteAttributes
	//
	//-----------------------------------------------------------------------------
	void TraitPointPlan::updateAnglesWriteAttributes()
	{
		//If there is no exception launched we can update the values.

		attr_pitch_write	= pitch;
		attr_zC_write		= zc;
		attr_roll_write		= roll;

		if (iType ==  TWO_MOTORS) 
			attr_roll_write =0;

		set_wattribute("pitch",attr_pitch_write);
		set_wattribute("zC",attr_zC_write - centerToHeight);
		set_wattribute("roll",attr_roll_write);

		if (iType == FIVE_MOTORS) //Five motors
		{
			attr_yaw_write	= yaw;
			attr_xC_write	= xc;
			set_wattribute("yaw",attr_yaw_write);
			set_wattribute("xC",attr_xC_write);
		}
	}

	//+------------------------------------------------------------------
	/**
	*	method:	set_attribute
	*
	*	description:	set an attribute write field to a desired value
	*
	*/
	//+------------------------------------------------------------------
	void TraitPointPlan::set_wattribute(string att_name, double value)
	{
		Tango::WAttribute &att = dev_attr->get_w_attr_by_name(att_name.c_str());
		att.set_write_value(value);	
		INFO_STREAM << "set write field of " <<  att_name << " attribute, to " << value << endl;
	}

	//+------------------------------------------------------------------
	/**
	*	method:	TraitPointPlan::stop
	*
	*	description:	method to execute "Stop"
	*	Stop all the mouvments. Useful when an invalid value to avoid to go to this position.
	*
	*
	*/
	//+------------------------------------------------------------------
	void TraitPointPlan::stop()
	{
		DEBUG_STREAM << "TraitPointPlan::stop(): entering... !" << endl;
		//	Add your own code to control device here

		//Stops all the motors.
		applyMotorsCommand(commandStopName);
	}

	//+------------------------------------------------------------------
	/**
	*	method:	TraitPointPlan::applyMotorsCommand
	*
	*	description:	method to execute applyMotorsCommand
	*	Execute a command on all axis
	*	@Param name of the command to be executed
	*
	*/
	//+------------------------------------------------------------------
	void TraitPointPlan::applyMotorsCommand(string sCommand)
	{
		size_t motor_ind;
		Tango::DeviceProxy *motor_proxy; 

		for( motor_ind = 0; motor_ind < tpp_attr_grp.device_proxy_list().size(); motor_ind++ )
		{
			try
			{
				motor_proxy = tpp_attr_grp.device_proxy_list()[motor_ind];
				motor_proxy->command_inout( sCommand );
			}
		catch( Tango::DevFailed &df )
		{
			ERROR_STREAM << "Tango::DevFailed error when executing command "<< sCommand << endl;
			ERROR_STREAM << df << endl;
		
		}
		catch(...)
		{
			ERROR_STREAM << "Unknown error when executing command "<< sCommand << endl;
		
			
		}
	} // end for
}

//+------------------------------------------------------------------
/**
*	method:	TraitPointPlan::undo
*
*	description:	method to execute "Undo"
*	Undo the last mouvement.
*
*
*/
//+------------------------------------------------------------------
void TraitPointPlan::undo()
{
	DEBUG_STREAM << "TraitPointPlan::undo(): entering... !" << endl;

	//	Add your own code to control device here
	//Stop all the motors
	this->stop();

	double t1ztemp = t1z;
	double t2ztemp = t2z;
	double t3ztemp = t3z;

	t1z = old_t1z;
	t2z = old_t2z;
	t3z = old_t3z;

	old_t1z = t1ztemp;
	old_t2z = t2ztemp;
	old_t3z = t3ztemp;

	if (iType == FIVE_MOTORS) 
	{
		double t4xtemp = t4x;
		double t5xtemp = t5x;

		t4x = old_t4x;
		t5x = old_t5x;

		old_t4x = t4xtemp;
		old_t5x = t5xtemp;	
	}

	sendValuesToMotors();
}

//+------------------------------------------------------------------
/**
*	method:	TraitPointPlan::ReadAllProxiesStates
*  description:	read state on all groups
*/
//+------------------------------------------------------------------
bool TraitPointPlan::ReadAllProxiesStates()
{
	bool _bAllProxiesStatesRead = true;
	size_t proxy_ind, proxy_num;
	Tango::DeviceProxy *current_proxy;
	Tango::DevState current_state;

	proxy_num = tpp_attr_grp.ordered_device_proxy_list().size();

	m_proxies_states.clear();

	for( proxy_ind = 0; proxy_ind < proxy_num ; proxy_ind++ )
	{
		try
		{

			current_proxy = tpp_attr_grp.ordered_device_proxy_list()[proxy_ind];
			current_state = current_proxy->state();


			m_proxies_states.push_back( current_state );
		}
		catch( Tango::DevFailed &df )
		{
			_bAllProxiesStatesRead = false;
			ERROR_STREAM << df << std::endl; 
			// set_state( Tango::FAULT );
			_sStatusMessage << "Proxy state read failed (see log for details )" << std::endl;
			m_proxies_states.push_back( ( Tango::DevState )-1 );
			throw;
		}
		catch(...)
		{
			_bAllProxiesStatesRead = false;
			//set_state( Tango::FAULT );
			_sStatusMessage << "Proxy state read failed (see log for details )" << std::endl;
			m_proxies_states.push_back( ( Tango::DevState )-1 );
			throw;
		}
	} 


	// Set a detailed State for each axis

	proxy_ind = 0;
	_mfront_elevation_state = m_proxies_states[proxy_ind++];
	if( _mfront_elevation_state == ( Tango::DevState )-1 )
	{
		ERROR_STREAM << "T1z state could not be read" << std::endl;
	}

	_mback_ext_elevation_state = m_proxies_states[proxy_ind++];
	if( _mback_ext_elevation_state == ( Tango::DevState )-1 )
	{
		ERROR_STREAM << "T2z state could not be read" << std::endl;
	}

	if (iType != TWO_MOTORS) 
	{
		_mback_int_elevation_state = m_proxies_states[proxy_ind++];
		if( _mback_int_elevation_state == ( Tango::DevState )-1 )
		{
			ERROR_STREAM << "T3z state could not be read" << std::endl;
		}
	}
	if (iType == FIVE_MOTORS) 
	{
		_mfront_translation_state = m_proxies_states[proxy_ind++];
		if( _mfront_translation_state == ( Tango::DevState )-1 )
		{
			ERROR_STREAM << "T4x state could not be read" << std::endl;
		}
		_mback_translation_state = m_proxies_states[proxy_ind++];
		if( _mback_translation_state == ( Tango::DevState )-1 )
		{
			ERROR_STREAM << "T5x state could not be read" << std::endl;
		}
	}

	return _bAllProxiesStatesRead;
}

//+------------------------------------------------------------------
/**
*	method:	TraitPointPlan::UpdateAllDeviceState
*
*  description: if every motor is on a different Tango device, display individual motor state and stats
*/
//+------------------------------------------------------------------
void TraitPointPlan::UpdateAllDeviceState()
{

	_sStatusMessage << frontElevationMotorName << " --> " << GetDeviceState(_mfront_elevation_state) << endl;
	_sStatusMessage << backExtElevationMotorName << " --> " << GetDeviceState(_mback_ext_elevation_state) << endl;
	if (iType != TWO_MOTORS) 
	{
		_sStatusMessage << backIntElevationMotorName << " --> " << GetDeviceState(_mback_int_elevation_state) << endl;
	}
	if (iType == FIVE_MOTORS)
	{
		_sStatusMessage << frontTranslationMotorName << " --> " << GetDeviceState(_mfront_translation_state) << endl;
		_sStatusMessage << backTranslationMotorName << " --> " << GetDeviceState(_mback_translation_state) << endl;
	}

}

//+------------------------------------------------------------------
/**
*	method:	TraitPointPlan::dev_state
*
*	description:	method to execute "State"
*	This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
*
* @return	State Code
*
*/
//+------------------------------------------------------------------
Tango::DevState TraitPointPlan::dev_state()
{
	Tango::DevState	argout = DeviceImpl::dev_state();
	DEBUG_STREAM << "TraitPointPlan::dev_state(): entering... !" << endl;

	_bBoundsException = false;
	stringstream    DeviceStatus; 
	DeviceStatus << "";
	Tango::DevState DeviceState		= Tango::ON;

	if (!m_initialised_group || !_bAllAttributesRead) ///There is a problem for initializing the proxies or to read the attribute from at least one proxy
	{
		DeviceState		= Tango::FAULT;
		DeviceStatus	<< _sStatusMessage.str();
	}
	else ///The proxies are ok and attributes of these proxies can be read.
	{
		if (!_bTraitPointPlanInitialized) ///There is a problem during the InitializeTraitPointPlan command.
		{
			DeviceState		= Tango::INIT;
			DeviceStatus	<< _sStatusMessage.str();
		}
		else ///The device is initialized and ready
		{
			//Check the proxies states
			//Trying to read the state of the motors

			bool state_set = false;
			bool standby_state = true;

			if (ReadAllProxiesStates())
			{

				for( size_t proxy_ind = 0; proxy_ind < m_proxies_states.size() && !state_set; proxy_ind++ )
				{

					if( m_proxies_states[proxy_ind] == Tango::OFF )
					{
						DeviceState	= Tango::OFF;
						DeviceStatus	<< "At least one motor of the TraitPointPlan is OFF...\n";
						UpdateAllDeviceState();
						DeviceStatus	<< _sStatusMessage.str();
						state_set = true;
					}
					else if( m_proxies_states[proxy_ind] == Tango::MOVING )
					{
						DeviceState	= Tango::MOVING;
						DeviceStatus	<< "At least one motor of the TraitPointPlan is MOVING...\n";
						UpdateAllDeviceState();
						DeviceStatus	<< _sStatusMessage.str();
						state_set = true;
					}
					else
					{
						standby_state = standby_state && ( m_proxies_states[proxy_ind] == Tango::STANDBY );
					}
				}

				if( state_set == false ) // state has not been set yet (i.e. state is not FAULT nor MOVING)
				{
					if( standby_state == true ) // all motors are in STANDBY state
					{
						DeviceState		= Tango::STANDBY;
						DeviceStatus	<< "All the motors are in STANDBY state...\n";
						DeviceStatus	<< _sStatusMessage.str();
					}
					else // At least one motor gives an state different from MOVING or STANDBY
					{
						DeviceState		= Tango::ALARM;
						DeviceStatus	<< "At least one motor is in ALARM state !!!\n";
						UpdateAllDeviceState();
						DeviceStatus	<< _sStatusMessage.str();
					}		
				}

				_sStatusMessage.str("");
			}
			else
			{
				DeviceState		= Tango::FAULT;
				UpdateAllDeviceState();
				DeviceStatus	<< _sStatusMessage.str();
				_sStatusMessage.str("");					//We clean the stringstream
				//The status is updated. In this case FAULT is the much more prioritary State
				set_state(DeviceState);
				set_status(DeviceStatus.str());
				return DeviceState;
			}

			try
			{
				// update positios and angles to detect a potential out of bounsd error
				update_positions_and_angles();
			}
			catch (ValueOutOfBoundException& e) 
			{
				ERROR_STREAM << "Exception in read_attr_hardware : " << e.getReason() << " " << e.getDescription() << " " << e.getOrigin() << endl;
				_sStatusMessage << "Exception in read_attr_hardware : " << e.getReason() << " " << e.getDescription() << " " << e.getOrigin() << endl;
				_bBoundsException = true;
			}
			catch(Tango::DevFailed& e)
			{	
				ERROR_STREAM << e << endl;
			}
			catch(...)
			{
				ERROR_STREAM << "Unknown Error in dev_state" << endl;
			}

			if (_bBoundsException)	///There is a problem due to a DivideByZeroException
			{
				DeviceState		= Tango::ALARM;
				UpdateAllDeviceState();
				DeviceStatus	<< _sStatusMessage.str();
				_sStatusMessage.str("");
			}
		}
	}


	//The status is updated.
	set_state(DeviceState);
	set_status(DeviceStatus.str());

	argout = DeviceState;

	return argout;
}

//+------------------------------------------------------------------
/**
*	method:	TraitPointPlan::save_as_nominal
*
*	description:	method to execute "SaveAsNominal"
*	Save the current values as the nominal ones (can be restored by the GoToNominal command).
*	The current Zc, Xc, Sc, Roll, Pitch, Yaw values are stored like nominal values.
*
*
*/
//+------------------------------------------------------------------
void TraitPointPlan::save_as_nominal()
{
	DEBUG_STREAM << "TraitPointPlan::save_as_nominal(): entering... !" << endl;


	//	Add your own code to control device here
	nominalZc	= *attr_zC_read - centerToHeight;
	nominalRoll	= *attr_roll_read;
	nominalPitch	= *attr_pitch_read;

	INFO_STREAM << "########################## SAVE DATA ############################" << endl;
	INFO_STREAM << "NominalZc --> \t" 		<< nominalZc << endl;		
	INFO_STREAM << "NominalRoll --> \t" 	<< nominalRoll << endl;
	INFO_STREAM << "NominalPitch --> \t" << nominalPitch << endl;

	Tango::DbData	data;
	Tango::DbDatum	ZcProperty("NominalZc");
	Tango::DbDatum	PitchProperty("NominalPitch");
	ZcProperty		<<  nominalZc;
	PitchProperty	<<  nominalPitch;
	data.push_back(ZcProperty);
	data.push_back(PitchProperty);

	if (iType != TWO_MOTORS)
	{
		Tango::DbDatum	RollProperty("NominalRoll");
		RollProperty	<<  nominalRoll;
		data.push_back(RollProperty);
	}

	if (iType == FIVE_MOTORS)
	{
		nominalYaw		= *attr_yaw_read;
		nominalXc		= *attr_xC_read;

		Tango::DbDatum	XcProperty("NominalXc");
		Tango::DbDatum	YawProperty("NominalYaw");

		XcProperty		<<  nominalXc;
		YawProperty		<<  nominalYaw;

		data.push_back(XcProperty);
		data.push_back(YawProperty);

		INFO_STREAM << "NominalYaw --> \t" 		<< nominalYaw << endl;		
		INFO_STREAM << "NominalXc --> \t" 	<< nominalXc << endl;
	}
	//	Call database and put values
	//--------------------------------------------
	get_db_device()->put_property(data);

	INFO_STREAM << "########################## END SAVE DATA ############################" << endl;
}

//+------------------------------------------------------------------
/**
*	method:	TraitPointPlan::go_to_nominal
*
*	description:	method to execute "GoToNominal"
*	Go to the nominal saved values (by the SaveAsNominal command).
*	The nominal values are stored in NominalZc, nominal Xc, nominalSc,
*	nominal Pitch, nominalYaw, nominal Roll.
*
*
*/
//+------------------------------------------------------------------
void TraitPointPlan::go_to_nominal()
{
	DEBUG_STREAM << "TraitPointPlan::go_to_nominal(): entering... !" << endl;

	//	Add your own code to control device here
	zc		= nominalZc + centerToHeight;
	pitch	= nominalPitch;
	INFO_STREAM << "########################## LOAD DATA ############################" << endl;
	INFO_STREAM << "NominalZc --> \t" 		<< nominalZc << " | (+centerToHeight) -->\t" << zc <<endl;		
	INFO_STREAM << "NominalPitch --> \t" << nominalPitch << endl;

	if (iType != TWO_MOTORS)
	{
		roll	= nominalRoll;
		INFO_STREAM << "NominalRoll --> \t" 	<< nominalRoll << endl;
	}

	if (iType == FIVE_MOTORS)
	{
		xc	= nominalXc;		
		yaw	= nominalYaw;

		INFO_STREAM << "NominalYaw --> \t" 		<< nominalYaw << endl;		
		INFO_STREAM << "NominalXc --> \t" 	<< nominalXc << endl;
	}

	INFO_STREAM << "########################## END LOAD DATA ############################" << endl;

	// Update write field of angle attributes with nominal values
	updateAnglesWriteAttributes();

	try 
	{	
		computeMotorsValues();
		checkMotorsBounds();
		updateMotorsWriteAttributes();
		sendValuesToMotors();
	}		
	catch (Exception& e) 
	{
		ERROR_STREAM << "Exception in go_to_nominal : " << e.getReason() << " " << e.getDescription() << " " << e.getOrigin() << endl;
		Tango::Except::throw_exception 
			(
			(const char *)(e.getReason()).c_str(),
			(const char *)(e.getDescription()).c_str(),
			(const char *)(e.getOrigin()).c_str()
			);
	}
	catch(Tango::DevFailed& e)
	{	
		ERROR_STREAM << e << endl;
		Tango::Except::re_throw_exception (	e,
			(const char *)"Dev_Failed",
			(const char *)"Exception Dev_Failed in go_to_nominal",
			(const char *)"go_to_nominal");
	}
	catch(...)
	{
		ERROR_STREAM << "Unknown Error in Exception in go_to_nominal " << endl;
	}
}

//+------------------------------------------------------------------
/**
*	method:	TraitPointPlan::motors_to_zero
*
*	description:	method to execute "MotorsToZero"
*	This command is used to make all the motors to the Zero position
*
*
*/
//+------------------------------------------------------------------
void TraitPointPlan::motors_to_zero()
{
	DEBUG_STREAM << "TraitPointPlan::motors_to_zero(): entering... !" << endl;

	//	Add your own code to control device here

	t1z = 0.0;
	t2z = 0.0;
	t3z = 0.0;

	if (iType == FIVE_MOTORS) 
	{
		t4x = 0.0;
		t5x = 0.0;
	}

	updateMotorsWriteAttributes();
	computeAngles();
	updateAnglesWriteAttributes();

	sendValuesToMotors();
}

//+----------------------------------------------------------------------------
//
// method : 		TraitPointPlan::init_attr_group()
// 
// description : 	register the attributes to be part of the attribute group
//
//-----------------------------------------------------------------------------
void TraitPointPlan::init_attr_group()
throw( Tango::DevFailed )
{
	std::vector<std::string> attr_vector;

	attr_vector.clear();

	attr_vector.push_back( frontElevationMotorName );
	attr_vector.push_back( backExtElevationMotorName );
	if (iType != TWO_MOTORS)
	{
		attr_vector.push_back( backIntElevationMotorName );
	}
	if (iType == FIVE_MOTORS)
	{
		attr_vector.push_back( frontTranslationMotorName );
		attr_vector.push_back( backTranslationMotorName );
	}

	try
	{
		tpp_attr_grp.register_attributes( attr_vector );
		m_initialised_group = true;

		for( int proxy_ind = 0; proxy_ind < attr_vector.size() ; proxy_ind++ )
		{
			//INFO_STREAM << "***** tpp_attr_grp.device_proxy_list()[" << proxy_ind << "] = " << tpp_attr_grp.device_proxy_list()[proxy_ind].name() << endl;
		}
	}
	catch( Tango::DevFailed &df )
	{
		ERROR_STREAM << df << endl;
		set_state( Tango::FAULT );
		_sStatusMessage << "Device is in FAULT state: Attribute group could not be initialised (see log for details )" << std::endl;
	}
	catch(...)
	{
		set_state( Tango::FAULT );
		_sStatusMessage << "Device is in FAULT state: Attribute group could not be initialised (check MotorName properties)" << std::endl;
	}
}

//+----------------------------------------------------------------------------
//
// method : 		TraitPointPlan::update_positions_and_angles()
// 
// description : 	read the motor positions and update the corresponding angle values
//
//-----------------------------------------------------------------------------
void TraitPointPlan::update_positions_and_angles()
{
	readAllMotors();
	updateMotorsTempValues();
	computeAngles();
	checkAnglesBounds();
	checkMotorsBounds();
}

//+----------------------------------------------------------------------------
//
// method : 		TraitPointPlan:init_if_needed( double &param , double defaultVal )
// 
// description : 	set unitiailaised parameters to their default values (depending on angle unit)
//
//-----------------------------------------------------------------------------
void TraitPointPlan::init_if_needed( double &param , double defaultVal )
{

	if( ( param == MIN_PROPERTIES_VALUE ) || ( param == MAX_PROPERTIES_VALUE ) )
	{
		if( m_angle_unit_degree == false )
		{
			param = defaultVal;
		}
		else
		{
			param = convert_mrad_to_deg( defaultVal );
		}
	}
}

double convert_mrad_to_deg( double mradAngle )
{
	double dResult = mradAngle / THOUSAND;
	dResult = dResult / PI * HALF_CIRCLE;

	return dResult;
}

double convert_deg_to_mrad( double degAngle )
{
	double dResult = degAngle * PI / HALF_CIRCLE;
	dResult = dResult * THOUSAND;

	return dResult;
}

}	//	namespace
