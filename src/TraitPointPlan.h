//=============================================================================
//
// file :        TraitPointPlan.h
//
// description : Include for the TraitPointPlan class.
//
// project :	Trait Point Plan Generic Device
//
// $Author: jcpret $
//
// $Revision: 1.30 $
//
// $Log: not supported by cvs2svn $
// Revision 1.29  2011/11/21 09:51:39  jcpret
// Fixed Mantis bug 20733: Angle unit can now be Deg or mRad (defined by AngleUnit property)
//
// Revision 1.28  2011/10/21 15:25:18  buteau
// Fix leak m�moire sur read_Zc
//
// Revision 1.27  2011/10/03 14:06:26  jcpret
// Removed InitializeReferencePosition Command (as requested by Mantis bug 19992)
//
// Revision 1.26  2011/09/08 14:12:54  buteau
// TEST 8/09/2011 sur SIXS
//
// Revision 1.25  2011/08/18 16:59:30  jcpret
// Adapted state management to AttributeGroup use (i.e. considered the case where several motors are on the same device)
//
// Revision 1.24  2011/08/09 14:48:46  jcpret
// Implementation of the recommendations defined in bug 18280
//
// Revision 1.23  2011/08/05 15:27:30  piccaf
// test SOLEIL 05/08/2011
//
// Revision 1.22  2011/08/02 09:50:01  jcpret
// first step of attributegroup integration
//
// Revision 1.21  2010/08/12 17:14:07  flanglois
// - set write part of attributes (mantis: 0015464)
// - cleaning in code
//
// Revision 1.20  2010/03/25 18:43:35  vince_soleil
// "Migration_Tango7"
//
// Revision 1.19  2007/06/05 12:05:47  stephpsoleil
// Adding Visual C++ 2005 workspace for Datafitter test device.
// Modifying sources to remove warning in visual 2005
//
// Revision 1.18  2007/05/29 10:08:53  stephpsoleil
// Auto creating properties in Jive if the property doesn't exist.
//
// Revision 1.17  2007/05/25 12:05:13  stephpsoleil
// Adding FirstInit property to create properties in Jive at the first device launch
//
// Updating documentation
//
// Revision 1.16  2007/03/19 11:21:35  stephpsoleil
// StateMachine verification and correction
// Status message improved : show the state of the reached device and the lost of communication
// Description of the properties, attributes, command and state.
// Removing eclipse workspace directory (now in the root of the directory)
//
// Revision 1.15  2007/03/16 14:55:06  stephpsoleil
// Dev_State improvment
// Description of the state of each proxy to a device or a motor
//
// Revision 1.14  2007/01/16 14:11:08  stephpsoleil
// Adding YawDirection properties
//
// Revision 1.13  2006/12/20 14:33:46  stephpsoleil
// Updating Eclipse Workspace
//
// Revision 1.12  2006/12/05 09:28:23  stephpsoleil
// Modification of the throwing signature to compile under Linux
//
// Revision 1.11  2006/12/04 13:12:51  stephpsoleil
// Removing doc + dev_state modification
//
// Revision 1.10  2006/12/01 15:59:50  stephpsoleil
// Update of the StateMachine
//
// Revision 1.9  2006/12/01 15:28:44  stephpsoleil
// New version of TPP to correct non coherent behaviour
//
// Revision 1.8  2006/11/21 12:58:07  stephpsoleil
// Modification for the min max values of device attribute
// Adding Config file
//
// Revision 1.7  2006/10/27 13:12:11  stephpsoleil
// Putting all possible attributes to memorized and removing write_hardware_at_init
//
// Defaults properties values are set when possible
//
// Revision 1.6  2006/07/06 08:22:09  stephpsoleil
// Sources update
//
// Revision 1.5  2006/06/27 13:31:44  stephpsoleil
// Updating makefiles and workspace
//
// Revision 1.4  2006/06/27 13:31:31  stephpsoleil
// Updating makefiles and workspace
//
// Revision 1.3  2006/03/23 09:51:22  stephpsoleil
// Added InitializeReferencePosition method
// Files Added
// Makefile.linux
// Scripts to launch the Motors and the TPP
// Script to launch AtkTuning
// Workspace Visual C++ 6.0
//
// Revision 1.2  2006/01/24 18:19:51  stephpsoleil
// Ajout de la doc.
// Modif �quations
// --> ajout sensRoll et sensPitch
// Correction du code (test sur TPP Samba)
//
//
// copyleft :    European Synchrotron Radiation Facility
//               BP 220, Grenoble 38043
//               FRANCE
//
//=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================
#ifndef _TRAITPOINTPLAN_H
#define _TRAITPOINTPLAN_H

#pragma warning( push )
#pragma warning( disable : 4312 )
#pragma warning( disable : 4311 )
#pragma warning( disable : 4267 )
#pragma warning( disable : 4290 )

#include <tango.h>
#include <helpers/TangoExceptionsHelper.h>

#include <yat4tango/AttributeGroup.h>

#pragma warning( pop )

#pragma warning( disable : 4290 )
#pragma warning( disable : 4250 )

#include <math.h>
//using namespace Tango;

#include "ValueOutOfBoundException.h"

/**
 * @author	$Author: jcpret $
 * @version	$Revision: 1.30 $
 */

 //	Add your own constants definitions here.
 //-----------------------------------------------


namespace TraitPointPlan_ns
{

/**
 * Class Description:
 * This class gives the implementation of the Trait Point Plan device.<BR>
 *	This device will convert 3 vertical translations and 2 horizontally translations (in the case of a five motors device) into 2 rotations (Pitch(Rx) and Roll(Rs)) and 1 translation (Tz).<BR>
 *	Moreover in the case of a five motors device, 1 rotation Yaw (Rz) and 1 translation (Tx).
 */

/*
 *	Device States Description:
 */


class TraitPointPlan: public Tango::Device_4Impl
{
public :

	enum { TWO_MOTORS,THREE_MOTORS, FIVE_MOTORS};

	//	Add your own data members here
	//-----------------------------------------
/*	Tango::DevDouble			roll_read;
	Tango::DevDouble			pitch_read;
	Tango::DevDouble			yaw_read;
	Tango::DevDouble			Z0_read;
	Tango::DevDouble			X0_read;
//	Tango::DevDouble			S0_read;
	Tango::DevDouble			Zc_read;
	Tango::DevDouble			Xc_read;
	Tango::DevDouble			dZ_read;
	Tango::DevDouble			dX_read;
		
	Tango::DevDouble			roll_write;
	Tango::DevDouble			pitch_write;
	Tango::DevDouble			yaw_write;
	Tango::DevDouble			Z0_write;
	Tango::DevDouble			X0_write;
//	Tango::DevDouble			S0_write;
	Tango::DevDouble			Zc_write;
	Tango::DevDouble			Xc_write;
	Tango::DevDouble			dZ_write;
	Tango::DevDouble			dX_write;
*/	
	

/*	Tango::DevDouble			t1z_read;
	Tango::DevDouble			t2z_read;
	Tango::DevDouble			t3z_read;
	Tango::DevDouble			t4x_read;
	Tango::DevDouble			t5x_read;
	
	Tango::DevDouble			t1z_write;
	Tango::DevDouble			t2z_write;
	Tango::DevDouble			t3z_write;
	Tango::DevDouble			t4x_write;
	Tango::DevDouble			t5x_write;
	
	Tango::DevDouble			old_t1z_write;
	Tango::DevDouble			old_t2z_write;
	Tango::DevDouble			old_t3z_write;
	Tango::DevDouble			old_t4x_write;
	Tango::DevDouble			old_t5x_write;
*/
	//	Here is the Start of the automatic code generation part
	//-------------------------------------------------------------	
/**
 *	@name attributes
 *	Attributs member data.
 */
//@{
		Tango::DevDouble	*attr_pitch_read;
		Tango::DevDouble	attr_pitch_write;
		Tango::DevDouble	*attr_roll_read;
		Tango::DevDouble	attr_roll_write;
		Tango::DevDouble	*attr_yaw_read;
		Tango::DevDouble	attr_yaw_write;
		Tango::DevDouble	*attr_zC_read;
		Tango::DevDouble	attr_zC_write;
		Tango::DevDouble	*attr_xC_read;
		Tango::DevDouble	attr_xC_write;
		Tango::DevDouble	*attr_t1z_read;
		Tango::DevDouble	attr_t1z_write;
		Tango::DevDouble	*attr_t2z_read;
		Tango::DevDouble	attr_t2z_write;
		Tango::DevDouble	*attr_t3z_read;
		Tango::DevDouble	attr_t3z_write;
		Tango::DevDouble	*attr_t4x_read;
		Tango::DevDouble	attr_t4x_write;
		Tango::DevDouble	*attr_t5x_read;
		Tango::DevDouble	attr_t5x_write;
//@}

/**
 *	@name Device properties
 *	Device properties member data.
 */
//@{
/**
 *	This property gives the name of the back exterior (to the ring) motor use to move for up to down
 *	and up to down the back exterior leg.
 */
	string	backExtElevationMotorName;
/**
 *	This property gives the name of the back interior motor use to move from up and down and
 *	down to up the back interior leg
 */
	string	backIntElevationMotorName;
/**
 *	This property gives the device name of the motor use to translate
 *	the two back legs from left to right and right to left.
 */
	string	backTranslationMotorName;
/**
 *	The value of the horizontal center to front
 */
	Tango::DevDouble	centerHorizontalToFront;
/**
 *	Distance between the center of the table at the bottom and the elevation of the table
 *	and his thickness.
 */
	Tango::DevDouble	centerToHeight;
/**
 *	Distance between the center of the table and the front leg. (The vertical one)
 */
	Tango::DevDouble	centerVerticalToFront;
/**
 *	The name of the command to check the state of the proxies.
 */
	string	commandStateName;
/**
 *	Name of the command to Stop all Axis (eg: AxisStop).
 */
	string	commandStopName;
/**
 *	The two available choices are :<BR>
 *	- 3 : the device is composed of three motors.<BR>
 *	- 5 : the device is composed of five motors.
 */
	string	deviceType;
/**
 *	This property gives the name of the front motor use to move for up to down
 *	and up to down the front leg.
 */
	string	frontElevationMotorName;
/**
 *	This property gives the device name of the motor use to translate
 *	the front leg from left to right and right to left.
 */
	string	frontTranslationMotorName;
/**
 *	Nominal value for pitch.
 */
	Tango::DevDouble	nominalPitch;
/**
 *	Nominal value for roll.
 */
	Tango::DevDouble	nominalRoll;
/**
 *	Nominal value for Xc.
 */
	Tango::DevDouble	nominalXc;
/**
 *	Nominal value for yaw.
 */
	Tango::DevDouble	nominalYaw;
/**
 *	Nominal value of the elevation Zc.
 */
	Tango::DevDouble	nominalZc;
/**
 *	Indicates the pitch sign to define if the pitch is positive or negative when the table
 *	roll toward up.
 *	The convention is the table pitch toward down (we look behind the beam)
 *	Convention = TRUE = 1.0
 *	FALSE = -1.0
 */
	Tango::DevBoolean	pitchDirection;
/**
 *	Indicates the roll sign to define if the roll is positive or negative when the table
 *	roll toward right.
 *	The convention is the table roll toward right (we look behind the beam)
 *	Convention = TRUE = 1.0
 *	FALSE = -1.0
 */
	Tango::DevBoolean	rollDirection;
/**
 *	Indicates the Yaw sign to define if the yaw is positive or negative when the table
 *	yaw toward right.
 *	The convention is the table yaw toward right (we look behind the beam)
 *	Convention = TRUE = 1.0
 *	FALSE = -1.0
 */
	Tango::DevBoolean	yawDirection;
/**
 *	The length of the table.
 */
	Tango::DevDouble	tableLength;
/**
 *	Width of the table.
 */
	Tango::DevDouble	tableWidth;
/**
 *	Unit to be used to expressed the angualr attrrbutes (Pitch, Rooll Yaw)
 *	Value is either "Deg" or mRad"
 */
	string	angleUnit;
//@}

/**@name Constructors
 * Miscellaneous constructors */
//@{
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	TraitPointPlan(Tango::DeviceClass *cl,string &s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	TraitPointPlan(Tango::DeviceClass *cl,const char *s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device name
 *	@param d	Device description.
 */
	TraitPointPlan(Tango::DeviceClass *cl,const char *s,const char *d);
//@}

/**@name Destructor
 * Only one desctructor is defined for this class */
//@{
/**
 * The object desctructor.
 */	
	~TraitPointPlan() {delete_device();};
/**
 *	will be called at device destruction or at init command.
 */
	void delete_device();
//@}

	
/**@name Miscellaneous methods */
//@{
/**
 *	Initialize the device
 */
	virtual void init_device();
/**
 *	Always executed method befor execution command method.
 */
	virtual void always_executed_hook();

//@}

/**
 * @name TraitPointPlan methods prototypes
 */

//@{
/**
 *	Hardware acquisition for attributes.
 */
	virtual void read_attr_hardware(vector<long> &attr_list);
/**
 *	Extract real attribute values for pitch acquisition result.
 */
	virtual void read_pitch(Tango::Attribute &attr);
/**
 *	Write pitch attribute values to hardware.
 */
	virtual void write_pitch(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for roll acquisition result.
 */
	virtual void read_roll(Tango::Attribute &attr);
/**
 *	Write roll attribute values to hardware.
 */
	virtual void write_roll(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for yaw acquisition result.
 */
	virtual void read_yaw(Tango::Attribute &attr);
/**
 *	Write yaw attribute values to hardware.
 */
	virtual void write_yaw(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for zC acquisition result.
 */
	virtual void read_zC(Tango::Attribute &attr);
/**
 *	Write zC attribute values to hardware.
 */
	virtual void write_zC(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for xC acquisition result.
 */
	virtual void read_xC(Tango::Attribute &attr);
/**
 *	Write xC attribute values to hardware.
 */
	virtual void write_xC(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for t1z acquisition result.
 */
	virtual void read_t1z(Tango::Attribute &attr);
/**
 *	Write t1z attribute values to hardware.
 */
	virtual void write_t1z(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for t2z acquisition result.
 */
	virtual void read_t2z(Tango::Attribute &attr);
/**
 *	Write t2z attribute values to hardware.
 */
	virtual void write_t2z(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for t3z acquisition result.
 */
	virtual void read_t3z(Tango::Attribute &attr);
/**
 *	Write t3z attribute values to hardware.
 */
	virtual void write_t3z(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for t4x acquisition result.
 */
	virtual void read_t4x(Tango::Attribute &attr);
/**
 *	Write t4x attribute values to hardware.
 */
	virtual void write_t4x(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for t5x acquisition result.
 */
	virtual void read_t5x(Tango::Attribute &attr);
/**
 *	Write t5x attribute values to hardware.
 */
	virtual void write_t5x(Tango::WAttribute &attr);
/**
 *	Read/Write allowed for pitch attribute.
 */
	virtual bool is_pitch_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for roll attribute.
 */
	virtual bool is_roll_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for yaw attribute.
 */
	virtual bool is_yaw_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for zC attribute.
 */
	virtual bool is_zC_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for xC attribute.
 */
	virtual bool is_xC_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for t1z attribute.
 */
	virtual bool is_t1z_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for t2z attribute.
 */
	virtual bool is_t2z_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for t3z attribute.
 */
	virtual bool is_t3z_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for t4x attribute.
 */
	virtual bool is_t4x_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for t5x attribute.
 */
	virtual bool is_t5x_allowed(Tango::AttReqType type);
/**
 *	Execution allowed for SaveAsNominal command.
 */
	virtual bool is_SaveAsNominal_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for GoToNominal command.
 */
	virtual bool is_GoToNominal_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for Stop command.
 */
	virtual bool is_Stop_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for Undo command.
 */
	virtual bool is_Undo_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for MotorsToZero command.
 */
	virtual bool is_MotorsToZero_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for InitializeTraitPointPlan command.
 */
	virtual bool is_InitializeTraitPointPlan_allowed(const CORBA::Any &any);
/**
 * This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
 *	@return	State Code
 *	@exception DevFailed
 */
	virtual Tango::DevState	dev_state();
/**
 * Save the current values as the nominal ones (can be restored by the GoToNominal command).
 *	The current Zc, Xc, Sc, Roll, Pitch, Yaw values are stored like nominal values.
 *	@exception DevFailed
 */
	void	save_as_nominal();
/**
 * Go to the nominal saved values (by the SaveAsNominal command).
 *	The nominal values are stored in NominalZc, nominal Xc, nominalSc,
 *	nominal Pitch, nominalYaw, nominal Roll.
 *	@exception DevFailed
 */
	void	go_to_nominal();
/**
 * Stop all the mouvments. Useful when an invalid value to avoid to go to this position.
 *	@exception DevFailed
 */
	void	stop();
/**
 * Undo the last mouvement.
 *	@exception DevFailed
 */
	void	undo();
/**
 * This command is used to make all the motors to the Zero position
 *	@exception DevFailed
 */
	void	motors_to_zero();
/**
 * Method to allow the initialization of the traitpointplan to a reference state.
 *	@exception DevFailed
 */
	void	initialize_trait_point_plan();

/**
 *	Read the device properties from database
 */
	 void get_device_property();
//@}

	//	Here is the end of the automatic code generation part
	//-------------------------------------------------------------	
	
private :

	double	getMinValue(std::string sAttributeName);
	double	getMaxValue(std::string sAttributeName);

	void	readAllMotors();
	void	sendValuesToMotors() throw (Tango::DevFailed);
	void	updateMotorsWriteAttributes();
	void	computeMotorsValues();
	void	updateMotorsTempValues();
        void    applyMotorsCommand(string sCommand);
	
        void	updateAnglesReadAttributes();
	void	updateAnglesWriteAttributes();
	void	computeAngles();

	void	updateAllTempValues();
	void	updateAnglesTempValues();
	
	bool	ReadAllProxiesStates();

	double	t1z;
	double	t2z;
	double	t3z;
	double	t4x;
	double	t5x;
	double	roll;
	double  pitch;
	double  yaw;
	double	xc;
	double  zc;

	double	old_t1z;
	double	old_t2z;
	double	old_t3z;
	double	old_t4x;
	double	old_t5x;

      yat4tango::AttributeGroup tpp_attr_grp;

	void checkMotorsBounds()	throw (ValueOutOfBoundException);
	void checkAnglesBounds()	throw (ValueOutOfBoundException);

	void checkRollBounds(double dRoll);
	void checkPitchBounds(double dPitch);
	void checkYawBounds(double dYaw);
	void checkT1zBounds(double dT1z);
	void checkT2zBounds(double dT2z);
	void checkT3zBounds(double dT3z);
	void checkT4xBounds(double dT4x);
	void checkT5xBounds(double dT5x);
	void checkZcBounds(double dZc);
	void checkXcBounds(double dXc);
	void	read3MotorsBounds();
	void	read5MotorsBounds();
	void	readMotorsBounds();

	Tango::DevState	_mfront_elevation_state;
	Tango::DevState	_mback_ext_elevation_state;
	Tango::DevState	_mback_int_elevation_state;
	Tango::DevState	_mfront_translation_state;
	Tango::DevState	_mback_translation_state;


	bool	_bBoundsException;
	bool	_bAllAttributesRead;
	bool	_bTraitPointPlanInitialized;
  bool     m_initialised_group;
  bool     m_critical_properties_missing;
  bool m_angle_unit_degree;

	
	stringstream				_sStatusMessage;

	void	UpdateAllDeviceState();

	void			CreateAllProperties(Tango::DbData& dev_prop);
	int			FindIndexFromPropertyName(Tango::DbData& dev_prop, string property_name);
	void			store_value_as_property(Tango::DbData& dev_prop,Tango::DevString value,string property_name);
	//	Add your own data members here
	//-----------------------------------------

	int iType;
	void set_wattribute(string att_name, double value);
  
  void update_positions_and_angles();

  void init_attr_group()    throw( Tango::DevFailed );
  void init_if_needed( double &param , double defaultVal );

  std::vector<Tango::DevState> m_proxies_states;

	double rollSign;
	double pitchSign;
	double yawSign;

	double	dMinimalRoll;
	double	dMaximalRoll;
	double	dMinimalPitch;
	double	dMaximalPitch;
	double	dMinimalYaw;
	double	dMaximalYaw;
	double	dMinimalT1z;
	double	dMaximalT1z;
	double	dMinimalT2z;
	double	dMaximalT2z;
	double	dMinimalT3z;
	double	dMaximalT3z;
	double	dMinimalT4x;
	double	dMaximalT4x;
	double	dMinimalT5x;
	double	dMaximalT5x;

	double	dMinimalZc;
	double	dMaximalZc;
	double	dMinimalXc;
	double	dMaximalXc;
	double  zcValTemp;

};

double convert_mrad_to_deg( double mradAngle );
double convert_deg_to_mrad( double degAngle );

}	// namespace_ns

#endif	// _TRAITPOINTPLAN_H
