//=============================================================================
//
// file :         TraitPointPlanClass.h
//
// description :  Include for the TraitPointPlanClass root class.
//                This class is the singleton class for
//                the TraitPointPlan device class.
//                It contains all properties and methods which the 
//                TraitPointPlan requires only once e.g. the commands.
//			
// project :      TANGO Device Server
//
// $Author: jcpret $
//
// $Revision: 1.13 $
// $Date: 2011-10-03 14:06:26 $
//
// SVN only:
// $HeadURL: $
//
// CVS only:
// $Source: /users/chaize/newsvn/cvsroot/Motion/TraitPointPlan/src/TraitPointPlanClass.h,v $
// $Log: not supported by cvs2svn $
// Revision 1.12  2011/09/08 14:12:54  buteau
// TEST 8/09/2011 sur SIXS
//
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================

#ifndef _TRAITPOINTPLANCLASS_H
#define _TRAITPOINTPLANCLASS_H

#include <tango.h>
#include <TraitPointPlan.h>


namespace TraitPointPlan_ns
{//=====================================
//	Define classes for attributes
//=====================================
class t5xAttrib: public Tango::Attr
{
public:
	t5xAttrib():Attr("t5x", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~t5xAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<TraitPointPlan *>(dev))->read_t5x(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<TraitPointPlan *>(dev))->write_t5x(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<TraitPointPlan *>(dev))->is_t5x_allowed(ty);}
};

class t4xAttrib: public Tango::Attr
{
public:
	t4xAttrib():Attr("t4x", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~t4xAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<TraitPointPlan *>(dev))->read_t4x(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<TraitPointPlan *>(dev))->write_t4x(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<TraitPointPlan *>(dev))->is_t4x_allowed(ty);}
};

class t3zAttrib: public Tango::Attr
{
public:
	t3zAttrib():Attr("t3z", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~t3zAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<TraitPointPlan *>(dev))->read_t3z(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<TraitPointPlan *>(dev))->write_t3z(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<TraitPointPlan *>(dev))->is_t3z_allowed(ty);}
};

class t2zAttrib: public Tango::Attr
{
public:
	t2zAttrib():Attr("t2z", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~t2zAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<TraitPointPlan *>(dev))->read_t2z(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<TraitPointPlan *>(dev))->write_t2z(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<TraitPointPlan *>(dev))->is_t2z_allowed(ty);}
};

class t1zAttrib: public Tango::Attr
{
public:
	t1zAttrib():Attr("t1z", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~t1zAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<TraitPointPlan *>(dev))->read_t1z(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<TraitPointPlan *>(dev))->write_t1z(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<TraitPointPlan *>(dev))->is_t1z_allowed(ty);}
};

class xCAttrib: public Tango::Attr
{
public:
	xCAttrib():Attr("xC", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~xCAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<TraitPointPlan *>(dev))->read_xC(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<TraitPointPlan *>(dev))->write_xC(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<TraitPointPlan *>(dev))->is_xC_allowed(ty);}
};

class zCAttrib: public Tango::Attr
{
public:
	zCAttrib():Attr("zC", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~zCAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<TraitPointPlan *>(dev))->read_zC(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<TraitPointPlan *>(dev))->write_zC(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<TraitPointPlan *>(dev))->is_zC_allowed(ty);}
};

class yawAttrib: public Tango::Attr
{
public:
	yawAttrib():Attr("yaw", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~yawAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<TraitPointPlan *>(dev))->read_yaw(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<TraitPointPlan *>(dev))->write_yaw(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<TraitPointPlan *>(dev))->is_yaw_allowed(ty);}
};

class rollAttrib: public Tango::Attr
{
public:
	rollAttrib():Attr("roll", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~rollAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<TraitPointPlan *>(dev))->read_roll(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<TraitPointPlan *>(dev))->write_roll(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<TraitPointPlan *>(dev))->is_roll_allowed(ty);}
};

class pitchAttrib: public Tango::Attr
{
public:
	pitchAttrib():Attr("pitch", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~pitchAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<TraitPointPlan *>(dev))->read_pitch(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<TraitPointPlan *>(dev))->write_pitch(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<TraitPointPlan *>(dev))->is_pitch_allowed(ty);}
};

//=========================================
//	Define classes for commands
//=========================================
class InitializeTraitPointPlanClass : public Tango::Command
{
public:
	InitializeTraitPointPlanClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	InitializeTraitPointPlanClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~InitializeTraitPointPlanClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<TraitPointPlan *>(dev))->is_InitializeTraitPointPlan_allowed(any);}
};



class MotorsToZeroClass : public Tango::Command
{
public:
	MotorsToZeroClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	MotorsToZeroClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~MotorsToZeroClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<TraitPointPlan *>(dev))->is_MotorsToZero_allowed(any);}
};



class UndoClass : public Tango::Command
{
public:
	UndoClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	UndoClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~UndoClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<TraitPointPlan *>(dev))->is_Undo_allowed(any);}
};



class StopClass : public Tango::Command
{
public:
	StopClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	StopClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~StopClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<TraitPointPlan *>(dev))->is_Stop_allowed(any);}
};



class LoadContextClass : public Tango::Command
{
public:
	LoadContextClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	LoadContextClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~LoadContextClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<TraitPointPlan *>(dev))->is_GoToNominal_allowed(any);}
};



class SaveContextClass : public Tango::Command
{
public:
	SaveContextClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	SaveContextClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~SaveContextClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<TraitPointPlan *>(dev))->is_SaveAsNominal_allowed(any);}
};



//
// The TraitPointPlanClass singleton definition
//

class
#ifdef WIN32
	__declspec(dllexport)
#endif
	TraitPointPlanClass : public Tango::DeviceClass
{
public:
//	properties member data

//	add your own data members here
//------------------------------------

public:
	Tango::DbData	cl_prop;
	Tango::DbData	cl_def_prop;
	Tango::DbData	dev_def_prop;

//	Method prototypes
	static TraitPointPlanClass *init(const char *);
	static TraitPointPlanClass *instance();
	~TraitPointPlanClass();
	Tango::DbDatum	get_class_property(string &);
	Tango::DbDatum	get_default_device_property(string &);
	Tango::DbDatum	get_default_class_property(string &);
	
protected:
	TraitPointPlanClass(string &);
	static TraitPointPlanClass *_instance;
	void command_factory();
	void get_class_property();
	void attribute_factory(vector<Tango::Attr *> &);
	void write_class_property();
	void set_default_property();
	string get_cvstag();
	string get_cvsroot();

private:
	void device_factory(const Tango::DevVarStringArray *);
};


}	//	namespace TraitPointPlan_ns

#endif // _TRAITPOINTPLANCLASS_H
