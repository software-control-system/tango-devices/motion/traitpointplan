#ifndef TPP_CONFIG_H
#define TPP_CONFIG_H


namespace TPP
{
	static const string NAME							="TPP";
	static const double MINIMAL_ROLL					= -100.0;
	static const double MAXIMAL_ROLL					= 100.0;

	static const double MINIMAL_PITCH					= -100.0;
	static const double MAXIMAL_PITCH					= 100.0;

	static const double MINIMAL_YAW						= -1000;
	static const double MAXIMAL_YAW						= 1000.0;

	static const double MINIMAL_T1Z						= -1000.0;
	static const double MAXIMAL_T1Z						= 1000.0;
	static const double MINIMAL_T2Z						= -1000.0;
	static const double MAXIMAL_T2Z						= 1000.0;
	static const double MINIMAL_T3Z						= -1000;
	static const double MAXIMAL_T3Z						= 1000;

	static const double MINIMAL_ZC						= -1000.0;
	static const double MAXIMAL_ZC						= 1000.0;
	static const double MINIMAL_XC						= -1000;
	static const double MAXIMAL_XC						= 1000;

	static const double MINIMAL_T4X						= -1000;
	static const double MAXIMAL_T4X						= 1000;
	static const double MINIMAL_T5X						= -1000;
	static const double MAXIMAL_T5X						= 1000;
	

	static const double CENTER_HORIZONTAL_TO_FRONT		= 50.0;
	static const double CENTER_TO_HEIGHT				= 1000.0;
	static const double CENTER_VERTICAL_TO_FRONT		= 50.0;

	static const double NOMINAL_PITCH					= 0.0;
	static const double NOMINAL_ROLL					= 0.0;
	static const double NOMINAL_XC						= 0.0;
	static const double NOMINAL_ZC						= 0.0;
	static const double NOMINAL_YAW						= 0.0;

	static const bool	PITCH_DIRECTION					= true;
	static const bool	ROLL_DIRECTION					= true;
	static const bool	YAW_DIRECTION					= true;

	static const double TABLE_LENGTH					= 100.0;
	static const double TABLE_WIDTH						= 20.0;

  static const string ANGLE_UNIT            = "mRad";


	namespace DEVICE_INIT_PROPERTIES
	{
		
		static const string COMMAND_STOP_NAME							= "Stop";
		static const string COMMAND_STATE_NAME							= "State";
		static const string ATTRIBUTE_POSITION_NAME						= "position";

	}

}



#endif

